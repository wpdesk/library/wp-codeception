<?php
/**
 * Class SettingsException
 * @package WPDesk\Composer\Codeception\Commands
 */

namespace WPDesk\Composer\Codeception\Commands;

/**
 * Settings Exception.
 */
class SettingsException extends \RuntimeException {
}
