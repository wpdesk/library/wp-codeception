<?php

namespace WPDesk\Composer\Codeception;

use WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests;
use WPDesk\Composer\Codeception\Commands\PrepareCodeceptionDb;
use WPDesk\Composer\Codeception\Commands\PrepareLocalCodeceptionTests;
use WPDesk\Composer\Codeception\Commands\PrepareLocalCodeceptionTestsWithCoverage;
use WPDesk\Composer\Codeception\Commands\PrepareParallelCodeceptionTests;
use WPDesk\Composer\Codeception\Commands\PrepareWordpressForCodeception;
use WPDesk\Composer\Codeception\Commands\RunCodeceptionTests;
use WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests;
use WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTestsWithCoverage;

/**
 * Links plugin commands handlers to composer.
 */
class CommandProvider implements \Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [
            new CreateCodeceptionTests(),
            new RunCodeceptionTests(),
            new RunLocalCodeceptionTests(),
            new RunLocalCodeceptionTestsWithCoverage(),
            new PrepareCodeceptionDb(),
            new PrepareWordpressForCodeception(),
            new PrepareLocalCodeceptionTests(),
            new PrepareLocalCodeceptionTestsWithCoverage(),
            new PrepareParallelCodeceptionTests(),
        ];
    }
}
