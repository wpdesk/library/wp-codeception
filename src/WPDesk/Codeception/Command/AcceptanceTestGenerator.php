<?php

namespace WPDesk\Codeception\Command;

use Codeception\Lib\Generator\Test;

/**
 * Class code for codeception example test for WP Desk plugin activation.
 *
 * @package WPDesk\Codeception\Command
 */
class AcceptanceTestGenerator extends Test
{

    protected $template = <<<EOF
<?php {{namespace}}

use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForPluginActivation;

class {{name}} extends AbstractCestForPluginActivation {

	/**
	 * Deactivate plugins before tests.
	 *
	 * @param AcceptanceTester \$i .
	 *
	 * @throws \Codeception\Exception\ModuleException .
	 */
	public function _before( \$i ) {
		\$i->loginAsAdministrator();
		\$i->amOnPluginsPage();
		\$i->deactivatePlugin( \$this->getPluginSlug() );
		\$i->amOnPluginsPage();
		\$i->seePluginDeactivated( \$this->getPluginSlug() );
		\$i->amOnPluginsPage();
		\$i->deactivatePlugin( self::WOOCOMMERCE_PLUGIN_SLUG );
		\$i->amOnPluginsPage();
		\$i->seePluginDeactivated( self::WOOCOMMERCE_PLUGIN_SLUG );
	}

	/**
	 * Plugin activation.
	 *
	 * @param AcceptanceTester \$i .
	 *
	 * @throws \Codeception\Exception\ModuleException .
	 */
	public function pluginActivation( \$i ) {

		\$i->loginAsAdministrator();

		\$i->amOnPluginsPage();
		\$i->seePluginDeactivated( \$this->getPluginSlug() );

		// This is an example and you should change it to current plugin.
		\$i->activateWPDeskPlugin(
			\$this->getPluginSlug(),
			array( 'woocommerce' ),
			array( 'The “WooCommerce Fakturownia” plugin cannot run without WooCommerce active. Please install and activate WooCommerce plugin.' )
		);

	}
}
EOF;

}
