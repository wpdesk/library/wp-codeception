<?php

namespace WPDesk\Codeception\Tests\Acceptance;

use AcceptanceTester;
use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForWpdeskTracker;

/**
 * Parent class for WPDesk Helper Tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 *
 * @deprecated Since version 1.4.
 * @deprecated Use AbstractCestForWpdeskTracker
 * @see AbstractCestForWpdeskTracker
 */
class WpdeskTracker extends AbstractCestForWpdeskTracker
{

}
