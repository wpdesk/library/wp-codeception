<?php

namespace WPDesk\Codeception\Tests\Acceptance;

use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForWooCommerce;

/**
 * Parent class for WooCommerce Tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 *
 * @deprecated Since version 1.4.
 * @deprecated Use AbstractCestForWooCommerce
 * @see AbstractCestForWooCommerce
 */
class WooCommerceCest extends AbstractCestForWooCommerce
{

}
