<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

abstract class AbstractCestForHelper extends AbstractCest
{

    /**
     * Prepare Key Activation Message.
     *
     * @param string $plugin_title .
     *
     * @return string
     */
    protected function prepareKeyActivationMessage()
    {
        return sprintf(
            "The %s API Key has not been activated, so you won't be supported and your plugin won't be updated! Click here to activate the API key and the plugin.",
            $this->getPluginProductId()
        );
    }

    /**
     * Run before each tests.
     *
     * @param \AcceptanceTester $i .
     * @throws \Codeception\Exception\ModuleException
     */
    public function _before($i)
    {
        $i->loginAsAdministrator();
        $i->amOnAdminPage('');
    }

    /**
     * Helper admin menu exists.
     *
     * @param \AcceptanceTester $i .
     *
     */
    public function helperMenu($i)
    {
        $i->click('WP Desk', ['id' => 'toplevel_page_wpdesk-helper']);
        $i->see('Subscriptions', "//a[contains(@href,'admin.php?page=wpdesk-licenses')]");
        $i->see('Settings', "//a[contains(@href,'admin.php?page=wpdesk-helper-settings')]");
    }

    /**
     * Plugin subscription exists.
     *
     * @param \AcceptanceTester $i .
     *
     */
    public function wpdeskSubscription($i)
    {
        $i->amOnAdminPage('admin.php?page=wpdesk-licenses');
        $i->see($this->getPluginProductId(), 'strong');
        $i->see('Deactivated', 'td');
        $i->seeElement('input', ['name' => 'api_key']);
        $i->seeElement('input', ['name' => 'activation_email']);
        $i->see('Activate', 'button');
    }

    /**
     * WP Desk Settings page exists and contains required elements.
     *
     * @param \AcceptanceTester $i .
     *
     */
    public function wpdeskSettings($i)
    {
        $i->amOnAdminPage('admin.php?page=wpdesk-helper-settings');

        $i->see('Debug', 'h2');
        $i->see('WP Desk Debug Log', 'th');
        $i->seeElement('input', [ 'id' => 'wpdesk_helper_options[debug_log]' ]);

        $i->see('Plugin usage tracking', 'h2');
        $i->see('Allow WP Desk to track plugin usage', 'th');
        $i->seeElement('input', [ 'id' => 'wpdesk_helper_options[wpdesk_tracker_agree]' ]);
    }

    /**
     * API Key activation message.
     *
     * @param \AcceptanceTester $i .
     *
     */
    public function keyActivationMessage($i)
    {
        $i->dontHaveOptionInDatabase('wpdesk_notice_dismiss_notice-' . $this->getPluginProductId());
        $i->see(
            $this->prepareKeyActivationMessage($this->getPluginTitle()),
            "//div[@data-notice-name='notice-" . $this->getPluginProductId() . "']"
        );
        $i->click(
            'Click here',
            "//div[@data-notice-name='notice-" . $this->getPluginProductId() . "']"
        );
        $i->see('WP Desk Subscriptions');
        $i->amOnAdminPage('');
        $i->click(
            "//div[@data-notice-name='notice-" . $this->getPluginProductId() . "']/button[@class='notice-dismiss']"
        );
        $i->wait(3);
        $i->amOnAdminPage('');
        $i->dontSee(
            $this->prepareKeyActivationMessage($this->getPluginTitle()),
            "//div[@data-notice-name='notice-" . $this->getPluginProductId() . "']"
        );
    }

}
