<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

/**
 * Parent class for WooCommerce tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Cest
 */
abstract class AbstractCestForWooCommerce extends AbstractCest
{

    const DEFAULT_FIRST_NAME = 'First';
    const DEFAULT_LAST_NAME = 'Last';
    const DEFAULT_ADDRESS_1 = 'Address 1';
    const DEFAULT_PHONE = '123456789';
    const DEFAULT_EMAIL = 'test@example.com';
    const DEFAULT_CITY = 'City';
    const DEFAULT_COUNTRY = 'PL';
    const DEFAULT_POSTCODE = '00-001';

    /**
     * Before.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function _before($i)
    {
        $i->enableWoocommerceTaxes();
    }

    /**
     * Make WooCommerce Order in Block Checkout.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException
     */
    public function makeOrderInBlockCheckout($i) {
        $i->switchCartAndCheckoutToBlocks();
        $this->makeOrder($i);
    }

    /**
     * Make WooCommerce Order in Old Checkout.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException
     */
    public function makeOrderInOldCheckout($i) {
        $i->switchCartAndCheckoutToOldVersion();
        $this->makeOrder($i);
    }

    /**
     * Make WooCommerce Order.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException
     */
    protected function makeOrder($i)
    {
        $i->logOutAndLoginAsCustomer();

        $i->emptyCart();

        $i->addProductsToCart(['product-009', 'product-09', 'product-9']);

        $order_id = $i->makeOrder();

        $i->logOutAndLoginAsAdministrator();
        $i->amOnAdminPage('post.php?post=' . $order_id . '&action=edit');
        $i->scrollTo('#woocommerce-order-items');

        if ( $i->grabOptionFromDatabase( 'woocommerce_custom_orders_table_enabled' ) === 'yes' ) {
            $i->seeInDatabase(
                'wp_wc_orders',
                ['id' => $order_id, 'total_amount' => 13.52]
            );
        } else {
            $i->seePostMetaInDatabase(['post_id' => $order_id, 'meta_key' => '_order_total', 'meta_value' => 13.52]);
        }

        $i->seeInDatabase(
            'wp_woocommerce_order_items',
            ['order_id' => $order_id, 'order_item_name' => 'Product 9', 'order_item_type' => 'line_item']
        );
        $i->seeInDatabase(
            'wp_woocommerce_order_items',
            ['order_id' => $order_id, 'order_item_name' => 'Product 09', 'order_item_type' => 'line_item']
        );
        $i->seeInDatabase(
            'wp_woocommerce_order_items',
            ['order_id' => $order_id, 'order_item_name' => 'Product 009', 'order_item_type' => 'line_item']
        );
    }

}
