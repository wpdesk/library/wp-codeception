<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

abstract class AbstractCestForPluginActionLinks extends AbstractCest
{

    /**
     * Settings URL.
     *
     * @var string
     */
    protected $settings_url = 'overwrite it';

    /**
     * Settings link label.
     *
     * @var string
     */
    protected $settings_label = 'Settings';

    /**
     * Expected text on settings page.
     *
     * @var string
     */
    protected $settings_page_text;

    /**
     * Documentation URL.
     *
     * @var string
     */
    protected $documentation_url = 'overwrite it';

    /**
     * Documentation link label.
     *
     * @var string
     */
    protected $documentation_link_label = 'Documentation';

    /**
     * Documentation UTM.
     *
     * @var string
     */
    protected $documentation_utm = '';

    /**
     * Support URL.
     *
     * @var string
     */
    protected $support_url = 'overwrite it';

    /**
     * Support link label.
     *
     * @var string
     */
    protected $support_link_label = 'Support';

    /**
     * Login as admin.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function _before($i)
    {
        $i->haveOptionInDatabase('WPLANG', 'en_US');
        $i->loginAsAdministrator();
        $i->amOnPluginsPage();
    }

    /**
     * Settings link on plugins page.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function settingsLinkOnPluginsPage($i)
    {
        $i->see(
            $this->settings_label,
            "//tr[@data-slug='" . $this->getPluginSlug() . "']//a[contains(@href, '" . $this->settings_url . "')]"
        );
        $i->click($this->settings_label, "//tr[@data-slug='" . $this->getPluginSlug() . "']");
        $i->see($this->settings_page_text);
        $i->amOnPluginsPage();
    }

    /**
     * Docs link on plugins page.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function docsLinkOnPluginsPage($i)
    {
        $i->see(
            $this->documentation_link_label,
            "//tr[@data-slug='" . $this->getPluginSlug() . "']//a[@href='" . $this->documentation_url . $this->documentation_utm . "' and @target='_blank']");
    }

    /**
     * Support link on plugins page.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function supportLinkOnPluginsPage($i)
    {
        $i->see(
            $this->support_link_label,
            "//tr[@data-slug='" . $this->getPluginSlug() . "']//a[@href='" . $this->support_url . "' and @target='_blank']"
        );
    }

}
