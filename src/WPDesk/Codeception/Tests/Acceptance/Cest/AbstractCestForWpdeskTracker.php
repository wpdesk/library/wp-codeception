<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

/**
 * Parent class for WPDesk Tracker tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Cest
 */
abstract class AbstractCestForWpdeskTracker extends AbstractCest
{

    const DEACTIVATE = 'Deactivate';
    const SKIP_AND_DEACTIVATE = 'Skip & Deactivate';
    const YOU_ARE_DEACTIVATING_PLUGIN = 'You are deactivating %1$s plugin.';
    const TR_DATA_SLUG = '//tr[@data-slug="%1$s"]';
    const H2_DEACTIVATION = '//h2[contains(text(),"%1$s")]';

    /**
     * Is deactivation with tracker questions?
     *
     * @var bool
     */
    protected $is_activation_with_tracker_questions = false;

    /**
     * Activate plugin after test.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function _after($i)
    {
        $i->amOnPluginsPage();
        $i->activatePlugin($this->getPluginSlug());
    }

   /**
     * Plugin deactivation.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function pluginDeactivation($i)
    {
        $i->loginAsAdministrator();

        $i->amOnPluginsPage();
        $i->seePluginActivated($this->getPluginSlug());

        $i->click(self::DEACTIVATE, sprintf(self::TR_DATA_SLUG, $this->getPluginSlug()));

        if ($this->is_activation_with_tracker_questions) {
            $i->seeNumberOfElements(sprintf(self::H2_DEACTIVATION, $this->getDeactivatingMessage()), 1);
            $i->click(self::SKIP_AND_DEACTIVATE);
            $i->seePluginDeactivated($this->getPluginSlug());
        } else {
            $i->dontSee($this->getDeactivatingMessage());
        }
    }

    /**
     * Get deactivation message.
     *
     * @return string
     */
    protected function getDeactivatingMessage()
    {
        return sprintf(self::YOU_ARE_DEACTIVATING_PLUGIN, $this->getPluginTitle());
    }

}
