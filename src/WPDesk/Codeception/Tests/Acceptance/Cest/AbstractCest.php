<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use WPDesk\Composer\Codeception\Commands\Configuration;

/**
 * Parent class for WPDesk acceptance tests.
 * Handles plugin slug from env variable.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Cest
 */
abstract class AbstractCest
{

    /**
     * Plugin slug.
     *
     * @var string
     */
    protected $pluginSlug = '';

    /**
     * Plugin file.
     *
     * @var string
     */
    protected $pluginFile = '';

    /**
     * Plugin title.
     *
     * @var string
     */
    protected $pluginTitle = '';

    /**
     * Plugin product ID.
     *
     * @var string
     */
    protected $pluginProductId = '';

    /**
     * Configuration.
     *
     * @var Configuration
     */
    protected $configuration;

    public function __construct()
    {
        $this->configuration = $this->createConfiguration();
        $this->configuration->prepareEnvForConfiguration();
        $this->pluginSlug = getenv('WPDESK_PLUGIN_SLUG');
        $this->pluginFile = getenv('WPDESK_PLUGIN_FILE');
        $this->pluginTitle = getenv('WPDESK_PLUGIN_TITLE');
        $this->pluginProductId = getenv('WPDESK_PLUGIN_PRODUCT_ID');
    }

    private function createConfiguration(): Configuration {
        try {
            $wpdesk_configuration = Yaml::parseFile( getcwd() . '/tests/codeception/wpdesk.yml' );
        } catch ( ParseException $e ) {
            $wpdesk_configuration = array();
        }

        return Configuration::createFromEnvAndConfiguration( $wpdesk_configuration );
    }


    /**
     * @return string
     */
    protected function getPluginSlug()
    {
        return $this->pluginSlug;
    }

    /**
     * @return string
     */
    protected function getPluginFile()
    {
        return $this->pluginFile;
    }

    /**
     * @return string
     */
    protected function getPluginTitle()
    {
        return $this->pluginTitle;
    }

    /**
     * @return string
     */
    protected function getPluginProductId()
    {
        return $this->pluginProductId;
    }

    /**
     * @param string $pluginSlug
     */
    protected function setPluginSlug($pluginSlug)
    {
        $this->pluginSlug = $pluginSlug;
    }

    /**
     * @param string $pluginFile
     */
    protected function setPluginFile($pluginFile)
    {
        $this->pluginFile = $pluginFile;
    }

    /**
     * @param string $pluginTitle
     */
    protected function setPluginTitle($pluginTitle)
    {
        $this->pluginTitle = $pluginTitle;
    }

    /**
     * @param string $pluginProductId
     */
    protected function setPluginProductId($pluginProductId)
    {
        $this->pluginProductId = $pluginProductId;
    }

}
