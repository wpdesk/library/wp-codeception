<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use WPDesk\Composer\Codeception\Commands\Configuration;
use WPDesk\Composer\Codeception\Commands\Language;

/**
 * Parent class for WPDesk acceptance tests.
 * Handles plugin slug from env variable.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Cest
 */
abstract class AbstractCestForCommonWordpressTests extends AbstractCest
{

    /**
     * Test translations.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException
     */
    public function testPluginTranslations( \AcceptanceTester $i ) {
        /** @var Language $language */
        foreach ( $this->configuration->getLanguages() as $language ) {
            $i->haveOptionInDatabase('WPLANG', $language->getLocale());
            $i->logOutAndLoginAsAdministrator();
            $i->amOnPluginsPage();
            $i->scrollTo('//tr[@data-slug=\''.$language->getPluginSlug().'\']');
            $i->see($language->getPluginTitle(), '//tr[@data-slug=\''.$language->getPluginSlug().'\']');
            if ($language->getPluginDescription() !== '') {
                $i->see($language->getPluginDescription(), '//tr[@data-slug=\''.$language->getPluginSlug().'\']');
            }
        }
    }

}
