<?php

namespace WPDesk\Codeception\Tests\Acceptance\Cest;

/**
 * Parent class for plugin activation tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Cest
 */
abstract class AbstractCestForPluginActivation extends AbstractCest
{

    const WOOCOMMERCE_PLUGIN_SLUG = 'woocommerce';

    /**
     * Prepare required plugin message text.
     *
     * @param string $current_plugin_title
     * @param string $required_plugin_title
     * @return string
     */
    protected function prepareRequiredPluginMessage($current_plugin_title, $required_plugin_title)
    {
        return sprintf(
            'The “%1$s” plugin cannot run without %2$s active. Please install and activate %3$s plugin.',
            $current_plugin_title,
            $required_plugin_title,
            $required_plugin_title
        );
    }

    /**
     * Plugin activation date in options.
     *
     * @param \AcceptanceTester $i .
     *
     * @throws \Codeception\Exception\ModuleException .
     */
    public function pluginActivationDateInOptions($i)
    {
        $option_name = 'plugin_activation_' . $this->getPluginFile();
        $i->dontHaveOptionInDatabase($option_name);

        $i->amOnPluginsPage();
        $i->activateSinglePlugin(self::WOOCOMMERCE_PLUGIN_SLUG);
        $i->seePluginActivated(self::WOOCOMMERCE_PLUGIN_SLUG);

        $i->deactivatePlugin($this->getPluginSlug());
        $i->activateSinglePlugin($this->getPluginSlug());
        $i->seePluginActivated($this->getPluginSlug());

        $i->seeOptionInDatabase(['option_name' => $option_name]);
    }


}
