<?php


namespace WPDesk\Codeception\Tests\Acceptance\Gherkin;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use Codeception\Step\Action;
use WPDesk\Codeception\Tests\Acceptance\Tester\TesterWordpressActions;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceCategoryTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceCheckoutTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommercePaymentsTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceProductTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceTagTrait;

/**
 * Gherkin steps generated.
 */
trait Steps {

    use AcceptanceTesterActions;
	use WPBrowserGherkinSteps;
	use WPDbGherkinSteps;
	use TesterWordpressActions;
	use TesterWooCommerceCheckoutTrait;
	use TesterWooCommerceProductTrait;
	use TesterWooCommercePaymentsTrait;
	use TesterWooCommerceTagTrait;
	use TesterWooCommerceCategoryTrait;
	use FlexibleShippingSteps;
	use ShippingMethodSteps;

	/**
	 *
	 * Wait for $timeout seconds.
	 *
	 * @Given I wait :timeout seconds
	 * @When  I wait :timeout seconds
	 * @Then  I wait :timeout seconds
	 *
	 * @param int|float $timeout secs
	 *
	 * @throws TestRuntimeException
	 * @see   \Codeception\Module\WebDriver::wait()
	 */
	public function step_wait( $timeout ) {
		return $this->wait( $timeout );
	}

	/**
	 *
	 * Waits up to $timeout seconds for the given string to appear on the page.
	 *
	 * @Given I wait for text :text
	 * @Given I wait for text :text the :timeout seconds
	 * @Given I wait for text :text the :timeout seconds in selector :selector
	 * @When  I wait for text :text
	 * @When  I wait for text :text the :timeout seconds
	 * @When  I wait for text :text the :timeout seconds in selector :selector
	 * @Then  I wait for text :text
	 * @Then  I wait for text :text the :timeout seconds
	 * @Then  I wait for text :text the :timeout seconds in selector :selector
	 *
	 * @param int|float $timeout secs
	 *
	 * @throws TestRuntimeException
	 * @see   \Codeception\Module\WebDriver::wait()
	 */
	public function step_wait_for_text( $text, $timeout = 10, $selector = null ) {
		return $this->waitForText( $text, $timeout, $selector );
	}

	/**
	 *
	 * Waits up to $timeout seconds for the given element to be clickable.
	 *
	 * @Given I wait for clickable element :element
	 * @When  I wait for clickable element :element the :timeout seconds
	 * @When  I wait for clickable element :element
	 * @When  I wait for clickable element :element the :timeout seconds
	 * @Then  I wait for clickable element :element
	 * @Then  I wait for clickable element :element the :timeout seconds
	 *
	 * @param string    $element .
	 * @param int|float $timeout secs
	 *
	 * @throws TestRuntimeException
	 */
	public function step_wait_for_element_clickable( $element, $timeout = 10 ) {
		return $this->waitForElementClickable( $element, $timeout );
	}

	/**
	 *
	 * Waits up to $timeout seconds for an element to appear on the page.
	 *
	 * @Given I wait for element :element
	 * @When  I wait for element :element the :timeout seconds
	 * @When  I wait for element :element
	 * @When  I wait for element :element the :timeout seconds
	 * @Then  I wait for element :element
	 * @Then  I wait for element :element the :timeout seconds
	 *
	 * @param string    $element .
	 * @param int|float $timeout secs
	 *
	 * @throws TestRuntimeException
	 */
	public function step_wait_for_element( $element, $timeout = 10 ) {
		return $this->waitForElement( $element, $timeout );
	}

	/**
	 * Empty cart
	 *
	 * @Given I have empty cart
	 * @When  I have empty cart
	 * @Then  I have empty cart
	 *
	 * @throws TestRuntimeException
	 */
	public function step_empty_cart() {
		$this->emptyCart();
	}

	/**
	 * Sets billing address fields in WooCommerce checkout.
	 *
	 * @Given I set billing address fields
	 * @When  I set billing address fields
	 * @Then  I set billing address fields
	 *
	 * @throws TestRuntimeException
	 */
	public function step_set_checkout_billing_fields() {
		$this->setCheckoutBillingFields();
	}

	/**
	 * Add products to card.
	 *
	 * @Given I add product :product to cart
	 * @When  I add product :product to cart
	 * @Then  I add product :product to cart
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_product_to_cart( $product ) {
		$this->addProductsToCart( explode( ',', $product ) );
	}

	/**
	 * Place order.
	 *
	 * @Given I place order
	 * @When  I place order
	 * @Then  I place order
	 *
	 * @throws TestRuntimeException
	 */
	public function step_place_order() {
		$this->placeOrder();
	}

	/**
	 * Go to checkout.
	 *
	 * @Given I am on checkout page
	 * @When  I am on checkout page
	 * @Then  I am on checkout page
	 *
	 * @throws TestRuntimeException
	 */
	public function step_am_on_checkout() {
		return $this->amOnPage( '/checkout' );
	}

	/**
	 * Add payment method - Direct bank transfer.
	 *
	 * @Given I add Direct bank transfer payment method
	 * @When  I add Direct bank transfer payment method
	 * @Then  I add Direct bank transfer payment method
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_payment_method_direct_transfer() {
		$this->enablePaymentGateway( 'bacs' );
	}

	/**
	 * Set category to product.
	 *
	 * @Given I set category :category_name on product :product_name
	 * @When  I set category :category_name on product :product_name
	 * @Then  I set category :category_name on product :product_name
	 *
	 * @throws TestRuntimeException
	 */
	public function step_set_category_on_product( $category_name, $product_name ) {
		$product  = $this->getProductByName( $product_name );
		$category = $this->getCategoryByName( $category_name );

		$this->updateProductCategory( $product['id'], $category['id'] );
	}

	/**
	 * Set tag to product.
	 *
	 * @Given I set tag :tag_name on product :product_name
	 * @When  I set tag :tag_name on product :product_name
	 * @Then  I set tag :tag_name on product :product_name
	 *
	 * @throws TestRuntimeException
	 */
	public function step_set_tag_on_product( $tag_name, $product_name ) {
		$product = $this->getProductByName( $product_name );
		$tag     = $this->getTagByName( $tag_name );

		$this->updateProductTag( $product['id'], $tag['id'] );
	}

	/**
	 * Set tag to product.
	 *
	 * @Given I delete all shipping methods
	 * @Given I delete all shipping methods in zone :zone_id
	 * @When  I delete all shipping methods
	 * @When  I delete all shipping methods in zone :zone_id
	 * @Then  I delete all shipping methods
	 * @Then  I delete all shipping methods in zone :zone_id
	 *
	 * @throws TestRuntimeException
	 */
	public function step_set_remove_all_shipping_methods( $zone_id = 0 ) {
		$this->deleteAllShippingMethodsInZone( $zone_id );
	}

    /**
     * Login as administrator.
     *
     * @Given I login as administrator
     * @When  I login as administrator
     * @Then  I login as administrator
     */
    public function step_logins_as_administrator() {
        $this->loginAsAdministrator();
    }
}
