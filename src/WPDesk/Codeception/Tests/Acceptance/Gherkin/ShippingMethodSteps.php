<?php

namespace WPDesk\Codeception\Tests\Acceptance\Gherkin;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceShippingTrait;

trait ShippingMethodSteps {
	use AcceptanceTesterActions;
	use TesterWooCommerceShippingTrait;

	/**
	 * Add Flat Rate
	 *
	 * @Given I add Flat Rate
	 * @Given I add Flat Rate with costs :costs
	 * @Given I add Flat Rate with costs :costs and zone :zone_id
	 * @When  I add Flat Rate
	 * @When  I add Flat Rate with costs :costs
	 * @When  I add Flat Rate with costs :costs and zone :zone_id
	 * @Then  I add Flat Rate
	 * @Then  I add Flat Rate with costs :costs
	 * @Then  I add Flat Rate with costs :costs and zone :zone_id
	 *
	 * @throws TestRuntimeException
	 */
    public function step_add_shipping_method_flat_rate( $costs = '0.0', $zone_id = 0 ) {
        return $this->createShippingMethodInZone( $zone_id, 'flat_rate', [ 'cost' => $costs ] );
    }

	/**
	 * Add Local Pickup
	 *
	 * @Given I add Local Pickup
	 * @Given I add Local Pickup in zone :zone_id
	 * @When  I add Local Pickup
	 * @When  I add Local Pickup in zone :zone_id
	 * @Then  I add Local Pickup
	 * @Then  I add Local Pickup in zone :zone_id
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_shipping_method_local_pickup( $zone_id = 0 ) {
		return $this->createShippingMethodInZone( $zone_id, 'local_pickup' );
	}

	/**
	 * Add Free Shipping
	 *
	 * @Given I add Free Shipping
	 * @Given I add Free Shipping in zone :zone_id
	 * @When  I add Free Shipping
	 * @When  I add Free Shipping in zone :zone_id
	 * @Then  I add Free Shipping
	 * @Then  I add Free Shipping in zone :zone_id
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_shipping_method_free_shipping( $zone_id = 0 ) {
		return $this->createShippingMethodInZone( $zone_id, 'free_shipping' );
	}
}
