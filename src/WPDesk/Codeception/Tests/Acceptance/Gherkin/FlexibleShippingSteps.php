<?php

namespace WPDesk\Codeception\Tests\Acceptance\Gherkin;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use Codeception\Step\Action;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceShippingTrait;

trait FlexibleShippingSteps {
	private $shipping_method_zone_id = null;
	private $shipping_method_method_id = null;
	private $shipping_method_instance_id = null;
	private $shipping_method_rule_id = null;

	use AcceptanceTesterActions;
	use TesterWooCommerceShippingTrait;

	/**
	 * Finish Onboarding
	 *
	 * @Given I finish FS Onboarding
	 * @When  I finish FS Onboarding
	 * @Then  I finish FS Onboarding
	 *
	 * @throws TestRuntimeException
	 */
	public function step_finish_fs_onboarding() {
		$args = [ 'flexible_shipping_onboarding_table_rate', '' ];

		return $this->getScenario()->runStep( new Action( 'haveOptionInDatabase', $args ) );
	}

	/**
	 * Add FS Shipping Method.
	 *
	 * @Given I add FS Shipping Method
	 * @Given I add FS Shipping Method to zone :zone_id
	 * @When  I add FS Shipping Method
	 * @When  I add FS Shipping Method to zone :zone_id
	 * @Then  I add FS Shipping Method
	 * @Then  I add FS Shipping Method to zone :zone_id
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_fs_shipping_method( $zone_id = 0 ) {
		$shipping_method = $this->createShippingMethodInZone( $zone_id, 'flexible_shipping_single' );

		$this->shipping_method_zone_id     = $zone_id;
		$this->shipping_method_method_id   = 'flexible_shipping_single';
		$this->shipping_method_instance_id = $shipping_method['id'];
		$this->shipping_method_rule_id     = null;
	}

	/**
	 *
	 * Add rule for FS Shipping Method.
	 *
	 * @Given I add FS rule with costs per rule :cost_per_order
	 * @When  I add FS rule with costs per rule :cost_per_order
	 * @Then  I add FS rule with costs per rule :cost_per_order
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_rule_fs_method( $cost_per_order ) {
		if ( null === $this->shipping_method_zone_id || null === $this->shipping_method_method_id || null === $this->shipping_method_instance_id ) {
			throw new TestRuntimeException( 'Please run "I add FS Shipping Method" before' );
		}

		$option_key = 'woocommerce_' . $this->shipping_method_method_id . '_' . $this->shipping_method_instance_id . '_settings';

		$settings = $this->grabOptionFromDatabase( $option_key );

		if ( is_string( $settings ) ) {
			$settings = [];
		}

		if ( ! isset( $settings['method_rules'] ) ) {
			$settings['method_rules'] = [];
		}

		$rule = [
			'conditions'     => [],
			'cost_per_order' => $cost_per_order,
		];

		$settings['method_rules'][] = $rule;

		$this->shipping_method_rule_id = count( $settings['method_rules'] ) - 1;

		$this->haveOptionInDatabase( $option_key, $settings );
	}

	/**
	 *
	 * Add rule for FS Shipping Method.
	 *
	 * @Given I add FS rule condition :condition_id
	 * @Given I add FS rule condition :condition_id with min :min and max :max
	 * @When  I add FS rule condition :condition_id
	 * @When  I add FS rule condition :condition_id with min :min and max :max
	 * @Then  I add FS rule condition :condition_id
	 * @Then  I add FS rule condition :condition_id with min :min and max :max
	 *
	 * @throws TestRuntimeException
	 */
	public function step_add_rule_condition_fs_method( $condition_id, $min = null, $max = null ) {
		if ( null === $this->shipping_method_rule_id ) {
			throw new TestRuntimeException( 'Please run "I add FS rule with costs per rule :cost_per_order" before' );
		}

		$option_key = 'woocommerce_' . $this->shipping_method_method_id . '_' . $this->shipping_method_instance_id . '_settings';

		$settings = $this->grabOptionFromDatabase( $option_key );

		$condition = [
			'condition_id' => $condition_id,
		];

		if ( $min ) {
			$condition['min'] = $min;
		}

		if ( $max ) {
			$condition['max'] = $max;
		}

		$settings['method_rules'][ $this->shipping_method_rule_id ]['conditions'][] = $condition;

		$this->haveOptionInDatabase( $option_key, $settings );
	}

	/**
	 *
	 * Add rule for FS Shipping Method.
	 *
	 * @Given I add FS Method config :key with value :value
	 * @When  I add FS Method config :key with value :value
	 * @Then  I add FS Method config :key with value :value
	 *
	 * @throws TestRuntimeException
	 */
	public function step_fs_method_config( $key, $value ) {
		if ( null === $this->shipping_method_zone_id || null === $this->shipping_method_method_id || null === $this->shipping_method_instance_id ) {
			throw new TestRuntimeException( 'Please run "I add FS Shipping Method" before' );
		}

		$option_key = 'woocommerce_' . $this->shipping_method_method_id . '_' . $this->shipping_method_instance_id . '_settings';

		$settings = $this->grabOptionFromDatabase( $option_key );

		if ( is_string( $settings ) ) {
			$settings = [];
		}

		$settings[ $key ] = $value;

		$this->haveOptionInDatabase( $option_key, $settings );
	}
}
