<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester;

use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceAdminNavigationTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceAdminSettingsGeneralTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceCheckoutTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceCouponsTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceShippingTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceToolsTrait;
use WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce\TesterWooCommerceCheckoutBlockTrait;

/**
 * Contains helper methods for WooCommerce.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceActions {
    use TesterWooCommerceAdminNavigationTrait;
    use TesterWooCommerceAdminSettingsGeneralTrait;
    use TesterWooCommerceCheckoutTrait;
    use TesterWooCommerceShippingTrait;
    use TesterWooCommerceToolsTrait;
    use TesterWooCommerceCouponsTrait;
    use TesterWooCommerceCheckoutBlockTrait;
}