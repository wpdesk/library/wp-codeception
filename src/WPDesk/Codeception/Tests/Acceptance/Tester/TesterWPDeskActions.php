<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester;

use WPDesk\Codeception\Tests\Acceptance\Tester\FlexibleShipping\TesterFlexibleShippingTrait;

/**
 * Contains helper methods for WP Desk.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWPDeskActions
{
    use TesterFlexibleShippingTrait;
}
