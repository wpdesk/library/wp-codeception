<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

/**
 * General settings tab in WooCommerce admin settings.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceAdminSettingsGeneralTrait {
    /**
     * WooCommerce option: woocommerce_calc_taxes.
     *
     * @var string
     */
    private $option_woocommerce_calc_taxes = 'woocommerce_calc_taxes';

    /**
     * Set shop address in WooCommerce admin.
     *
     * @param string $origin_address
     * @param string $origin_city
     * @param string $origin_country
     * @param string $origin_postcode
     */
    public function setShopAddress( $origin_address, $origin_city, $origin_country, $origin_postcode ) {
        $this->dontHaveOptionInDatabase( 'woocommerce_store_address' );
        $this->dontHaveOptionInDatabase( 'woocommerce_store_city' );
        $this->dontHaveOptionInDatabase( 'woocommerce_store_country' );
        $this->dontHaveOptionInDatabase( 'woocommerce_store_postcode' );
        $this->haveOptionInDatabase( 'woocommerce_store_address', $origin_address );
        $this->haveOptionInDatabase( 'woocommerce_store_city', $origin_city );
        $this->haveOptionInDatabase( 'woocommerce_default_country', $origin_country );
        $this->haveOptionInDatabase( 'woocommerce_store_postcode', $origin_postcode );
    }

    /**
     * Set shop weight unit in WooCommerce admin.
     *
     * @param string $weight_unit lowercase ie. lbs
     */
    public function setShopWeightUnit( $weight_unit ) {
        $this->dontHaveOptionInDatabase( 'woocommerce_weight_unit' );
        $this->haveOptionInDatabase( 'woocommerce_weight_unit', $weight_unit );
    }

    /**
     * Set shop currency unit in WooCommerce admin.
     *
     * @param string $currency upcase ie. GPB.
     * @param string $position dot position.
     */
    public function setShopCurrency( $currency, $position = 'right_space' ) {
        $this->dontHaveOptionInDatabase( 'woocommerce_currency' );
        $this->haveOptionInDatabase( 'woocommerce_currency', $currency );

        $this->dontHaveOptionInDatabase( 'woocommerce_currency_pos' );
        $this->haveOptionInDatabase( 'woocommerce_currency_pos', $position );
    }

    /**
     * Disable woocommerce taxes.
     */
    public function disableWoocommerceTaxes() {
        $this->dontHaveOptionInDatabase( $this->option_woocommerce_calc_taxes );
        $this->haveOptionInDatabase( $this->option_woocommerce_calc_taxes, 'no' );
    }

    /**
     * Enable woocommerce taxes.
     */
    public function enableWoocommerceTaxes() {
        $this->dontHaveOptionInDatabase( $this->option_woocommerce_calc_taxes );
        $this->haveOptionInDatabase( $this->option_woocommerce_calc_taxes, 'yes' );
    }

}
