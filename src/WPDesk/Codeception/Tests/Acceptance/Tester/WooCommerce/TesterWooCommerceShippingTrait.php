<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use Codeception\Util\Locator;
use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce shipping zones and methods.
 *
 * @see https://woocommerce.github.io/woocommerce-rest-api-docs/
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceShippingTrait
{

    use TesterWordpressCli;

    private $rest_api_zones_url = 'wp-json/wc/v3/shipping/zones';

    /**
     * List shipping zones.
     * Returns JSON with shipping zones.
     *
     * @return array
     */
    public function listShippingZones()
    {
        $this->sendGET(
            $this->rest_api_zones_url
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Create shipping zone.
     * Returns JSON with created shipping zone.
     *
     * @param string $zone_name .
     *
     * @return array
     */
    public function createShippingZone($zone_name)
    {
        $this->sendPOST(
            $this->rest_api_zones_url,
            array( 'name' => $zone_name )
        );
        $this->seeResponseCodeIs(201);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Update shipping zone location.
     * Returns JSON with shipping zone locations.
     *
     * @param int $zone_id .
     * @param array $locations .
     *
     * @return array
     */
    public function updateShippingZoneLocations($zone_id, $locations)
    {
        $this->sendPUT(
            $this->rest_api_zones_url . "/{$zone_id}/locations",
            $locations
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Delete shipping zone.
     * Returns JSON with deleted shipping zone.
     *
     * @param int $zone_id .
     *
     * @return array
     */
    public function deleteShippingZone($zone_id)
    {
        $this->sendDELETE(
            $this->rest_api_zones_url . "/{$zone_id}",
            array('force' => true)
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Delete all shipping zones.
     */
    public function deleteAllShippingZones()
    {
        $shipping_zones = $this->listShippingZones();
        foreach ($shipping_zones as $shipping_zone) {
            $zone_id = $shipping_zone['id'];
            if ($zone_id !== 0) {
                $this->deleteShippingZone($zone_id);
            }
        }
    }

    /**
     * List shipping methods in zone.
     * Returns array with shipping methods in zone.
     *
     * @param int $zone_id .
     *
     * @return array
     */
    public function listShippingMethodsInZone($zone_id)
    {
        $this->sendGET(
            $this->rest_api_zones_url . "/{$zone_id}/methods"
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Create shipping method in zone.
     * Returns array with created shipping method.
     *
     * @param int $zone_id .
     * @param string $method_id .
     *
     * @return array
     */
    public function createShippingMethodInZone($zone_id, $method_id = 'flexible_shipping', $settings = [])
    {
        $this->sendPOST(
            $this->rest_api_zones_url . "/{$zone_id}/methods",
	        array(
	            'method_id' => $method_id,
                'settings' => $settings,
            )
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Update shipping method in zone.
     * Returns array with updated shipping method.
     *
     * @param int $zone_id .
     * @param int $instance_id .
     * @param array $shipping_method_data .
     *
     * @return array
     */
    public function updateShippingMethodInZone($zone_id, $instance_id, array $shipping_method_data = array())
    {
        $this->sendPUT(
            $this->rest_api_zones_url . "/{$zone_id}/methods/{$instance_id}",
            $shipping_method_data
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Delete shipping method in zone.
     * Returns array with deleted shipping method.
     *
     * @param int $zone_id .
     * @param int $instance_id .
     *
     * @return array
     */
    public function deleteShippingMethodInZone($zone_id, $instance_id)
    {
        $this->sendDELETE(
            $this->rest_api_zones_url . "/{$zone_id}/methods/{$instance_id}",
            array('force' => true)
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Delete all shipping methods in zone.
     *
     * @param int $zone_id .
     *
     */
    public function deleteAllShippingMethodsInZone($zone_id)
    {
        //$this->disableWpDebug();
        $shipping_methods = $this->listShippingMethodsInZone($zone_id);
        foreach ($shipping_methods as $shipping_method) {
            $this->deleteShippingMethodInZone($zone_id, $shipping_method['instance_id']);
        }
        //$this->enableWpDebug();
    }

    /**
     * Add shipping method in current shipping zone.
     *
     * @param string $shipping_method .
     *
     */
    public function addShippingMethodInCurrentShippingZone( string $shipping_method ) {
        $this->waitForElementClickable( '//button[contains(text(),"Add shipping method")]' );
        $this->click( 'Add shipping method' );
        $this->clickWithLeftButton( '//label[@for="' . $shipping_method . '"]' );
        $this->waitForElementClickable( '//button[contains(text(),"Continue")]' );
        $this->click( 'Continue' );
    }

    /**
     * Add shipping method in shipping zone.
     *
     * @param string $shipping_method .
     * @param int $zone_id .
     *
     */
    public function addShippingMethodInShippingZone( string $shipping_method, int $zone_id ) {
        $this->amOnShippingZoneSettingsPage( $zone_id );
        $this->addShippingMethodInCurrentShippingZone( $shipping_method );
    }

    public function amOnShippingZoneSettingsPage( int $zone_id ) {
        $this->amOnAdminPage( 'admin.php?page=wc-settings&tab=shipping&zone_id=' . $zone_id );
    }

    public function amOnShippingZoneSettingsPageWithShippingMethod( int $zone_id, string $shipping_method ) {
        $this->amOnShippingZoneSettingsPage( $zone_id );
        $this->click( 'Edit', '//td[contains(text(), "' . $shipping_method . '")]/..' );
    }

    /**
     * Add shipping method in shipping zone and go to settings.
     *
     * @param string $shipping_method .
     * @param int $zone_id .
     * @param string $shipping_method_name .
     *
     */
    public function addShippingMethodInShippingZoneAndGoToSettings( string $shipping_method, int $zone_id, string $shipping_method_name ) {
        $this->addShippingMethodInShippingZone( $shipping_method, $zone_id );
        $this->amOnShippingZoneSettingsPageWithShippingMethod( $zone_id, $shipping_method_name );
    }

}
