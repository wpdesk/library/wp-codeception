<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce product.
 *
 * @see     https://woocommerce.github.io/woocommerce-rest-api-docs/
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceTagTrait {
	use TesterWordpressCli;
	use AcceptanceTesterActions;

	private $rest_api_product_tag_url = 'wp-json/wc/v3/products/tags';

	/**
	 * @param string $tag_name .
	 *
	 * @return array
	 */
	public function getTagByName( $tag_name ) {
		$this->sendGET(
			$this->rest_api_product_tag_url,
			[
				'search' => $tag_name,
			]
		);
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		$tags = json_decode( $this->grabResponse(), true );

		if ( empty( $tags ) ) {
            throw new TestRuntimeException( sprintf( 'Tag "%s" not found', $tag_name ) );
		}

		return $tags[0];
	}
}
