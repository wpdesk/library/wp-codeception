<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce product.
 *
 * @see     https://woocommerce.github.io/woocommerce-rest-api-docs/
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceCategoryTrait {
	use TesterWordpressCli;
	use AcceptanceTesterActions;

	private $rest_api_product_cat_url = 'wp-json/wc/v3/products/categories';

	/**
	 * @param string $category_name .
	 *
	 * @return array
	 */
	public function getCategoryByName( $category_name ) {
		$this->sendGET(
			$this->rest_api_product_cat_url,
			[
				'search' => $category_name,
			]
		);
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		$categories = json_decode( $this->grabResponse(), true );

		if ( empty( $categories ) ) {
			throw new TestRuntimeException( sprintf( 'Category "%s" not found', $category_name ) );
		}

		return $categories[0];
	}
}
