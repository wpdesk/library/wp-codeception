<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce checkout block.
 *
 * @see     https://woocommerce.github.io/woocommerce-rest-api-docs/
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceCheckoutBlockTrait {

	use TesterWordpressCli;

	/**
	 * Fill fields in checkout blocks
	 *
	 * @param AcceptanceTester $i
	 *
	 * @return void
	 */
	public function fillCheckoutBlock($address = array(
		'email' => 'test@test.com',
		'first_name' => 'Test',
		'last_name' => 'Test',
		'address' => 'Test 2',
		'country' => 'Poland',
		'postcode' => '31-982',
		'city' => 'Kraków'
	)) {
		if (!strlen($this->grabValueFrom('#email')) > 0) {
			$this->fillField('#email', $address['email']);
		}
		$this->fillField('#shipping-first_name', $address['first_name']);
		$this->fillField('#shipping-last_name', $address['last_name']);
		$this->fillField('#shipping-address_1', $address['address']);

		// fix for older Woo versions
		try {
			$this->selectOption('#shipping-country', $address['country']);
		} catch (Exception $e) {
			$this->fillField('#components-form-token-input-0', $address['country']);
		}

		$this->fillField('#shipping-postcode', $address['postcode']);
		$this->fillField('#shipping-city', $address['city']);
		$this->checkOption('Use same address for billing');
	}
}
