<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

/**
 * Navigation in WooCommerce admin.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceAdminNavigationTrait {

    /**
     * Go to WooCommerce admin/settings/general.
     */
    public function gotoAdminSettingsGeneral() {
        $this->amOnAdminPage( 'admin.php?page=wc-settings&tab=general' );
    }

}
