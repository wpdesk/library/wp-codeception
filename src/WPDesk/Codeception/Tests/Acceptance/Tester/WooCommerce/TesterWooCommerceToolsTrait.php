<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

/**
 * WooCommerce tools.
 *
 * @see https://woocommerce.github.io/woocommerce-rest-api-docs/
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceToolsTrait
{

    private $system_tools_url = 'wp-json/wc/v3/system_status/tools';

    /**
     * List WooCommerce system status tools.
     * Returns JSON with shipping zones.
     *
     * @return array
     */
    public function listWooCommerceSystemTools()
    {
        $this->sendGET(
            $this->system_tools_url
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

    /**
     * Run system tool.
     * Returns JSON with shipping zone locations.
     *
     * @param string $tool .
     * @param array $data .
     *
     * @return array
     */
    public function runWooCommerceTool($tool, $data = array())
    {
        $this->sendPUT(
            $this->system_tools_url . "/{$tool}",
            $data
        );
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();
        return json_decode($this->grabResponse(), true);
    }

}
