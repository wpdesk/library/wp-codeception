<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use Codeception\Util\Locator;
use Facebook\WebDriver\Exception\ElementNotInteractableException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForWooCommerce;
use WPDesk\Codeception\Tests\Acceptance\WooCommerceCest;

/**
 * WooCommerce checkout.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceCheckoutTrait
{

    use TesterWooCommerceCheckoutBlockTrait;

    private function getCountries()
    {
        return [
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        ];
    }

    private function emptyField( $field )
    {
        echo $field . "\n";
        echo $this->grabValueFrom($field) . "\n";
        while (strlen($this->grabValueFrom($field) ) > 0) {
            $this->pressKey($field, \Facebook\WebDriver\WebDriverKeys::BACKSPACE);
            $this->pressKey($field, \Facebook\WebDriver\WebDriverKeys::DELETE);
        }
    }

    /**
     * Sets billing address fields in WooCommerce checkout.
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $address_1
     * @param string $phone
     * @param string $city
     * @param string $country
     * @param string $postcode
     * @param string $email
     */
    public function setCheckoutBillingFields(
        $first_name = WooCommerceCest::DEFAULT_FIRST_NAME,
        $last_name = WooCommerceCest::DEFAULT_LAST_NAME,
        $address_1 = WooCommerceCest::DEFAULT_ADDRESS_1,
        $phone = WooCommerceCest::DEFAULT_PHONE,
        $city = WooCommerceCest::DEFAULT_CITY,
        $country = WooCommerceCest::DEFAULT_COUNTRY,
        $postcode = WooCommerceCest::DEFAULT_POSTCODE,
        $email = WooCommerceCest::DEFAULT_EMAIL
    ) {
        $block_checkout = count( $this->grabMultiple( 'div.wc-block-checkout' ) ) > 0;
        if ( ! $block_checkout ) {
            $this->waitForElement('input#billing_first_name');
            $this->fillField('billing_first_name', $first_name);
            $this->fillField('billing_last_name', $last_name);
            $this->fillField('billing_phone', $phone);
            $this->fillField('billing_email', $email);

            $this->fillField('billing_city', $city);
            $this->selectOption('billing_country', ['value' => $country]);
            $this->fillField('billing_postcode', $postcode);
            $this->scrollTo('input.search-field');
            $this->fillField('billing_city', $city);

            $this->fillField('billing_address_1', $address_1);
        } else {
            $has_edit_link = count( $this->grabMultiple( '.wc-block-components-address-card__edit' ) ) > 0;
            if ( $has_edit_link ) {
                try {
                    $this->waitForElementClickable('.wc-block-components-address-card__edit');
                    $this->click('.wc-block-components-address-card__edit');
                } catch (TimeoutException $e) {
                    // do nothing
                }
            }
            $this->waitForElement('input#shipping-first_name');
            $this->emptyField('input#shipping-first_name');
            $this->fillField('input#shipping-first_name', $first_name);
            $this->emptyField('input#shipping-last_name');
            $this->fillField('input#shipping-last_name', $last_name);
            $this->emptyField('input#shipping-phone');
            $this->fillField('input#shipping-phone', $phone);
            $this->emptyField('input#email');
            $this->fillField('input#email', $email);

            $this->emptyField('input#shipping-city');
            $this->fillField('input#shipping-city', $city);
            
            // fix for Woo 9.2
            try {
                $this->selectOption('#shipping-country', $this->getCountries()[$country]);
            } catch (ElementNotFound $e) {
                $this->fillField('#components-form-token-input-0', $this->getCountries()[$country]);
            }
            
            $this->emptyField('input#shipping-postcode');
            $this->fillField('input#shipping-postcode', $postcode);
            $this->emptyField('input#shipping-city');
            $this->fillField('input#shipping-city', $city);

            $this->emptyField('input#shipping-address_1');
            $this->fillField('input#shipping-address_1', $address_1);
        }
    }

    /**
     * Maybe set custom fields
     *
     * @param array $custom_fields Custom fields.
     */
    public function maybeSetCustomFields($custom_fields)
    {
        if (is_array($custom_fields) && count($custom_fields) > 0) {
            foreach ($custom_fields as $name => $value) {
                $this->fillField($name, $value);
            }
        }
    }

    /**
     * When checkout fields are changing the ajax request is blocking interaction. Wait for fully loaded page.
     */
    public function waitForCheckoutAjaxRender()
    {
        $this->waitForElementNotVisible('//div[@class="blockUI blockOverlay"]', 60);
        $this->wait(1);
        $this->waitForElementNotVisible('//div[@class="blockUI blockOverlay"]', 60);
    }

    /**
     * Add products to card.
     *
     * @param string|array $product_slugs .
     */
    public function addProductsToCart($product_slugs)
    {
        if (!is_array($product_slugs)) {
            $product_slugs = [$product_slugs];
        }
        $wait = 5;
        foreach ($product_slugs as $product_slug) {
            $this->amOnPage('/product/' . $product_slug);
            $this->wait($wait);
            $wait = 3;
            $this->waitForElementClickable('//button[@name="add-to-cart"]');
            $this->clickWithLeftButton('//button[@name="add-to-cart"]');
            $this->waitForText('has been added to your cart.');
        }
    }

    /**
     * Fill checkout.
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $address_1
     * @param string $phone
     * @param string $city
     * @param string $country
     * @param string $postcode
     * @param array $custom_fields
     * @param string $shipping_method
     */
    public function fillCheckout(
        $first_name = AbstractCestForWooCommerce::DEFAULT_FIRST_NAME,
        $last_name = AbstractCestForWooCommerce::DEFAULT_LAST_NAME,
        $address_1 = AbstractCestForWooCommerce::DEFAULT_ADDRESS_1,
        $phone = AbstractCestForWooCommerce::DEFAULT_PHONE,
        $city = AbstractCestForWooCommerce::DEFAULT_CITY,
        $country = AbstractCestForWooCommerce::DEFAULT_COUNTRY,
        $postcode = AbstractCestForWooCommerce::DEFAULT_POSTCODE,
        $custom_fields = [],
        $shipping_method = ''
    )
    {
        $this->amOnPage('/');
        $this->click('Shop');

        $this->click('Cart');
        $blockCart = count( $this->grabMultiple('div.wp-block-woocommerce-cart') ) > 0;
        if ( ! $blockCart ) {
            $this->waitForElementClickable('a.checkout-button');
            $this->wait(3);
            $this->clickWithLeftButton('a.checkout-button');
            $this->waitForElement('#ship-to-different-address-checkbox');
            $this->uncheckOption('#ship-to-different-address-checkbox');
        } else {
            $this->waitForElementClickable('a.wc-block-cart__submit-button');
            $this->clickWithLeftButton('a.wc-block-cart__submit-button');
            $this->waitForElement('input#email');
        }

        $this->setCheckoutBillingFields(
            $first_name,
            $last_name,
            $address_1,
            $phone,
            $city,
            $country,
            $postcode
        );
        $this->maybeSetCustomFields($custom_fields);


        if ('' !== $shipping_method) {
            $this->waitForCheckoutAjaxRender();
            $this->selectOption('input[name="shipping_method[0]"]', $shipping_method);
        }

        $this->waitForCheckoutAjaxRender();
        try {
            $this->waitForElementClickable('button[name=woocommerce_checkout_place_order]', 10);
        } catch (NoSuchElementException $e) {
            $this->waitForElementClickable('button.wc-block-components-checkout-place-order-button', 10);
        }
    }

    /**
     * Place order.
     *
     * @return int
     */
    public function placeOrder()
    {
	    $this->waitForCheckoutAjaxRender();
        try {
            $this->waitForElementClickable('button[name=woocommerce_checkout_place_order]');
            $this->click('button[name=woocommerce_checkout_place_order]');
        } catch (NoSuchElementException $e) {
            $this->waitForElementClickable('button.wc-block-components-checkout-place-order-button');
            $this->click('button.wc-block-components-checkout-place-order-button');
        }

        $this->waitForElement('li.woocommerce-order-overview__order', 60);
        $this->see('Order received', 'h1');

        return intval($this->grabFromCurrentUrl('~/order-received/(\d+)/~'));
    }

    /**
     * Make order.
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $address_1
     * @param string $phone
     * @param string $city
     * @param string $country
     * @param string $postcode
     * @param array $custom_fields
     * @param string $shipping_method
     *
     * @return int
     */
    public function makeOrder(
        $first_name = AbstractCestForWooCommerce::DEFAULT_FIRST_NAME,
        $last_name = AbstractCestForWooCommerce::DEFAULT_LAST_NAME,
        $address_1 = AbstractCestForWooCommerce::DEFAULT_ADDRESS_1,
        $phone = AbstractCestForWooCommerce::DEFAULT_PHONE,
        $city = AbstractCestForWooCommerce::DEFAULT_CITY,
        $country = AbstractCestForWooCommerce::DEFAULT_COUNTRY,
        $postcode = AbstractCestForWooCommerce::DEFAULT_POSTCODE,
        $custom_fields = [],
        $shipping_method = ''
    ) {
        $this->fillCheckout(
            $first_name,
            $last_name,
            $address_1,
            $phone,
            $city,
            $country,
            $postcode,
            $custom_fields,
            $shipping_method
        );

        return $this->placeOrder();
    }

    /**
     * Empty cart.
     */
    public function emptyCart()
    {
        $this->amOnPage( '/cart' );
        $blockCart = count( $this->grabMultiple('div.wp-block-woocommerce-cart') ) > 0;
        if ( $blockCart ) {
            $cart_items = $this->grabMultiple('//button[@class="wc-block-cart-item__remove-link"]');
        } else {
            $cart_items = $this->grabMultiple('//a[@class="remove"]');
        }
        while ( $cart_items ) {
            if ( $blockCart ) {
                $element = Locator::lastElement( '(//button[@class="wc-block-cart-item__remove-link"])[' . count( $cart_items ) . ']' );
                $this->click( $element );
                $this->waitForElementNotVisible( $element, 30 );
                $this->reloadPage();
                $cart_items = $this->grabMultiple('//button[@class="wc-block-cart-item__remove-link"]');
            } else {
                $element = Locator::lastElement( '(//a[@class="remove"])[' . count( $cart_items ) . ']' );
                $this->waitForElementClickable( $element, 30 );
                $this->click( $element );
                $this->waitForElementNotVisible( $element, 30 );
                $this->waitForBlockOverlayNotVisible();
                $this->wait( 2 );
                $cart_items = $this->grabMultiple('//a[@class="remove"]');
            }
        }
    }

    /**
     * Wait for block overlay div nit visible.
     */
    public function waitForBlockOverlayNotVisible()
    {
        $this->waitForElementNotVisible('div.blockOverlay', 30);
    }

    public function switchCartAndCheckoutToOldVersion()
    {
        $cart_content = '[woocommerce_cart]';
        $checkout_content = '[woocommerce_checkout]';

        $this->updateInDatabase($this->grabPostsTableName(), ['post_content' => $cart_content], ['post_name' => 'cart']);
        $this->updateInDatabase($this->grabPostsTableName(), ['post_content' => $checkout_content], ['post_name' => 'checkout']);
    }

    public function switchCartAndCheckoutToBlocks()
    {
        ob_start();
        include __DIR__ . '/views/cart.php';
        $cart_content = ob_get_clean();

        ob_start();
        include __DIR__ . '/views/checkout.php';
        $checkout_content = ob_get_clean();

        $this->updateInDatabase($this->grabPostsTableName(), ['post_content' => $cart_content], ['post_name' => 'cart']);
        $this->updateInDatabase($this->grabPostsTableName(), ['post_content' => $checkout_content], ['post_name' => 'checkout']);
    }

}
