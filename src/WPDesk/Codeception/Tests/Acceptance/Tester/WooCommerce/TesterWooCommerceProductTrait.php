<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use _generated\AcceptanceTesterActions;
use Codeception\Exception\TestRuntimeException;
use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce product.
 *
 * @see     https://woocommerce.github.io/woocommerce-rest-api-docs/
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommerceProductTrait {
	use TesterWordpressCli;
	use AcceptanceTesterActions;

	private $rest_api_products_url = 'wp-json/wc/v3/products';

	/**
	 * Update product category.
	 * Returns array with updated product.
	 *
	 * @param int $product_id  .
	 * @param int $category_id .
	 *
	 * @return array
	 */
	public function updateProductCategory( $product_id, $category_id ) {
		$params = [
			'categories' => [
				[
					'id' => $category_id,
				],
			]
		];

		return $this->updateProduct( $product_id, $params );
	}

	/**
	 * Update product tag.
	 * Returns array with updated product.
	 *
	 * @param int $product_id  .
	 * @param int $tag_id .
	 *
	 * @return array
	 */
	public function updateProductTag( $product_id, $tag_id ) {
		$params = [
			'tags' => [
				[
					'id' => $tag_id,
				],
			]
		];

		return $this->updateProduct( $product_id, $params );
	}

	/**
	 * Update product category.
	 * Returns array with updated product.
	 *
	 * @param int   $product_id .
	 * @param array $params     .
	 *
	 * @return array
	 */
	public function updateProduct( $product_id, $params ) {
		$this->sendPUT( $this->rest_api_products_url . "/{$product_id}", $params );
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		return json_decode( $this->grabResponse(), true );
	}

	/**
	 * @param string $product_name .
	 *
	 * @return array
	 */
	public function getProductByName( $product_name ) {
		$this->sendGET(
			$this->rest_api_products_url,
			[
				'search' => $product_name,
			]
		);
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		$products = json_decode( $this->grabResponse(), true );

		if ( empty( $products ) ) {
            throw new TestRuntimeException( sprintf( 'Product "%s" not found', $product_name ) );
        }

		return $products[0];
	}

	/**
	 * List products.
	 *
	 * @return array
	 */
	public function listProducts() {
		$this->sendGET(
			$this->rest_api_products_url
		);
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		return json_decode( $this->grabResponse(), true );
	}
}
