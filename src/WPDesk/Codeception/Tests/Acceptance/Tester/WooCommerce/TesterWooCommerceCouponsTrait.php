<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

/**
 * WooCommerce coupons.
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce
 */
trait TesterWooCommerceCouponsTrait {

    /**
     * .
     *
     * @param string $coupon_code .
     *
     * @throws \Exception
     */
    public function addCouponToCart( $coupon_code ) {
        $this->amOnPage( '/cart' );
        $this->fillField('coupon_code', $coupon_code );
        $this->click( 'Apply coupon' );
        $this->waitForText( 'Coupon code applied successfully.' );
    }

    /**
     * .
     *
     * @throws \Exception
     */
    public function removeCouponFromCart() {
        $this->amOnPage( '/cart' );
        $this->click( '[Remove]' );
    }

}
