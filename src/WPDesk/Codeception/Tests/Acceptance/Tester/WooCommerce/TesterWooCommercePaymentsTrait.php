<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\WooCommerce;

use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * WooCommerce shipping zones and methods.
 *
 * @see     https://woocommerce.github.io/woocommerce-rest-api-docs/
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWooCommercePaymentsTrait {

	use TesterWordpressCli;

	private $rest_api_payment_gateway_url = 'wp-json/wc/v3/payment_gateways';

	/**
	 * Update shipping zone location.
	 * Returns JSON with shipping zone locations.
	 *
	 * @param string $id .
	 *
	 * @return array
	 */
	public function enablePaymentGateway( $id ) {
		$this->sendPUT(
			$this->rest_api_payment_gateway_url . "/{$id}",
			[
				'enabled' => true,
			]
		);
		$this->seeResponseCodeIs( 200 );
		$this->seeResponseIsJson();

		return json_decode( $this->grabResponse(), true );
	}
}
