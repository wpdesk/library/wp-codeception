<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester\FlexibleShipping;

/**
 * Flexible shipping.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterFlexibleShippingTrait
{

    private $flexible_shipping_methods_counter = 1;

    /**
     * @param string $instance_id
     * @return string
     */
    private function _generateFlexibleShippingMethodsOptionName($instance_id)
    {
        return 'flexible_shipping_methods_' . $instance_id;
    }

    /**
     * Update Flexible Shipping Methods.
     *
     * @param int $instance_id .
     * @param array $methods_settings .
     */
    public function updateFlexibleShippingMethods($instance_id, $methods_settings)
    {
        $this->flexible_shipping_methods_counter++;
        $this->haveOptionIndatabase($this->_generateFlexibleShippingMethodsOptionName($instance_id), $methods_settings);
        $methods_order = array();
        foreach ($methods_settings as $key => $method_setting) {
            $methods_order[] = $key;
        }
        $this->haveOptionIndatabase('flexible_shipping_methods_order_' . $instance_id, $methods_order);
    }

    /**
     * Add Flexible Shipping method.
     *
     * @param int $instance_id
     * @param array $method_settings
     * @return array
     */
    public function addFlexibleShippingMethod($instance_id, array $method_settings)
    {
        $fs_methods = $this->grabOptionFromDatabase($this->_generateFlexibleShippingMethodsOptionName($instance_id));
        if (empty($fs_methods) && ! is_array($fs_methods)) {
            $fs_methods = array();
        }
        $method_settings['woocommerce_method_instance_id'] = $instance_id;
        $method_settings['id'] = $this->flexible_shipping_methods_counter;
        $method_settings['id_for_shipping'] = $this->flexible_shipping_methods_counter;
        $fs_methods[$this->flexible_shipping_methods_counter] = $method_settings;
        $this->flexible_shipping_methods_counter++;
        $this->updateFlexibleShippingMethods($instance_id, $fs_methods);
        return $method_settings;
    }

    /**
     * Update Flexible Shipping method.
     *
     * @param int $instance_id
     * @param int $method_id
     * @param array $method_settings
     * @return array
     */
    public function updateFlexibleShippingMethod($instance_id, $method_id, array $method_settings)
    {
        $fs_methods = $this->grabOptionFromDatabase($this->_generateFlexibleShippingMethodsOptionName($instance_id));
        if (empty($fs_methods) && ! is_array($fs_methods)) {
            $fs_methods = array();
        }
        $fs_methods[$method_id] = $method_settings;
        $this->updateFlexibleShippingMethods($instance_id, $fs_methods);
        return $method_settings;
    }

    /**
     * Go to Flexible Shipping instance settings.
     *
     * @param int $instance_id .
     */
    public function amOnFlexibleShippingSettings($instance_id)
    {
        $this->amOnAdminPage("admin.php?page=wc-settings&tab=shipping&instance_id={$instance_id}");
    }

    /**
     * Go to Flexible Shipping method settings.
     *
     * @param int $instance_id .
     * @param int|string $flexible_shipping_method_id .
     */
    public function amOnFlexibleShippingMethodSettings($instance_id, $flexible_shipping_method_id)
    {
        $this->amOnAdminPage("admin.php?page=wc-settings&tab=shipping&instance_id={$instance_id}&action=edit&method_id={$flexible_shipping_method_id}");
    }
}
