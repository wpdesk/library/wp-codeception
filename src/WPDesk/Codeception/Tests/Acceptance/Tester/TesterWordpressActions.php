<?php

namespace WPDesk\Codeception\Tests\Acceptance\Tester;

use _generated\AcceptanceTesterActions;
use WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress\TesterWordpressCli;

/**
 * Contains helper methods for Wordpress.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
trait TesterWordpressActions
{

    use TesterWordpressCli;

    /**
     * Activate WPDesk plugin.
     *
     * @param string $pluginSlug .
     * @param array $requiredPlugins .
     * @param array $messages .
     */
    public function activateWPDeskPlugin($pluginSlug, array $requiredPlugins, array $messages)
    {
        $this->amOnPluginsPage();

        $this->activateSinglePlugin($pluginSlug);

        $this->amOnPluginsPage();

        foreach ($messages as $message) {
            $this->see($message);
        }

        foreach ($requiredPlugins as $requiredPlugin) {
            $this->activateSinglePlugin($requiredPlugin);
        }

        $this->amOnPluginsPage();

        foreach ($messages as $message) {
            $this->dontSee($message);
        }
    }

    /**
     * Go to login page.
     */
    public function amOnLoginPage()
    {
        $this->amOnPage('wp-login.php');
    }

    /**
     * Login as customer.
     */
    public function loginAsCustomer()
    {
        $this->loginAs('customer', 'customer');
    }

    /**
     * Login as administrator.
     */
    public function loginAsAdministrator()
    {
        $this->loginAsAdmin();

        $elements = $this->grabMultiple( "//a[contains(text(),'Update WordPress Database')]" );
        if ( count( $elements ) ) {
            $this->click( 'Update WordPress Database' );
            $this->waitForText( 'Your WordPress database has been successfully updated!' );
            $this->click( 'Continue' );
        }
    }

    /**
     * Log out from WordPress.
     *
     * @param bool $force_logout .
     */
    public function goToLoginPageAndLogOut( $force_logout = false ) {
        $this->amOnPage( 'wp-login.php?action=logout' );
        try {
            $this->click('log out');
        } catch ( \Exception $e ) {
            $this->amOnLoginPage();
        }
    }

    /**
     * Logout and login as Customer.
     */
    public function logOutAndLoginAsCustomer() {
        $this->goToLoginPageAndLogOut();
        $this->loginAsCustomer();
    }

    /**
     * Logout and login as Administrator.
     */
    public function logOutAndLoginAsAdministrator() {
        $this->goToLoginPageAndLogOut();
        $this->loginAsAdministrator();
    }

    public function activateSinglePlugin($pluginSlug)
    {
        $this->amOnPluginsPage();
        $this->clickWithLeftButton( '//a[@id="activate-' . $pluginSlug . '"]' );
    }

    public function deactivateSinglePlugin($pluginSlug)
    {
        $this->amOnPluginsPage();
        $this->clickWithLeftButton( '//a[@id="deactivate-' . $pluginSlug . '"]' );
    }

}
