<?php
/**
 * Trait TesterWordpressCli
 *
 * @package WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress
 */

namespace WPDesk\Codeception\Tests\Acceptance\Tester\Wordpress;

/**
 * Can execute various WP Cli commands.
 */
trait TesterWordpressCli
{

    /**
     * @param array|string $command .
     *
     * @return string[]
     */
    public function executeWpCli( $command ) {
        if ( ! is_array( $command ) ) {
            $command = explode( ' ', $command );
        }

        return $this->cli( $command );
    }

    /**
     * @return void
     */
    public function disableWpDebug() {
        $this->executeWpCli( 'config set WP_DEBUG_DISPLAY false --raw' );
    }

    /**
     * @return void
     */
    public function enableWpDebug() {
        $this->executeWpCli( 'config set WP_DEBUG_DISPLAY true --raw' );
    }

}
