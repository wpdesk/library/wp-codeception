<?php

namespace WPDesk\Codeception\Tests\Acceptance;

use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForPluginActivation;

/**
 * Parent class for WPDesk plugin Activation tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 *
 * @deprecated Since version 1.4.
 * @deprecated Use CestForActivation
 * @see AbstractCestForPluginActivation
 */
abstract class ActivationCest extends AbstractCestForPluginActivation
{

}
