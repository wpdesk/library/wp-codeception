<?php

namespace WPDesk\Codeception\Tests\Acceptance\Elements;

/**
 * Collection of elements.
 * Tests can be run on whole collection, methods: iSeeElements, iDontSeeElements.
 */
class Elements
{

    /**
     * @var AbstractElement[];
     */
    private $elements = [];

    /**
     * @param AbstractElement $element
     * @param string $id
     */
    private function addElementToArray(AbstractElement $element, $id)
    {
         $this->elements[$id] = $element;
    }

    /**
     * @param AbstractElement $element .
     */
    public function removeElement(AbstractElement $element)
    {
        unset($this->elements[$element->getId()]);
    }

    /**
     * @param string $selector .
     * @param array $attributes .
     * @param string $id .
     *
     * @return Element
     */
    public function addElement($selector, array $attributes = [], $id = '')
    {
        $element = new Element($selector, $attributes, $id);
        $this->addElementToArray($element, $element->getId());
        return $element;
    }

    /**
     * @param string $text .
     * @param array|string $selector .
     * @param string $id
     *
     * @return Label
     */
    public function addLabel($text, $selector = '', $id = '')
    {
        $label = new Label($text, $selector);
        $this->addElementToArray($label, $label->getId());
        return $label;
    }

    /**
     * @param Elements $elements
     */
    public function addElements(Elements $elements)
    {
        foreach ($elements->elements as $element) {
            $this->addElementToArray($element, $element->getId());
        }
    }

    /**
     * @param Elements $elements
     */
    public function removeElements(Elements $elements)
    {
        foreach ($elements->elements as $element) {
            $this->removeElement($element);
        }
    }

    /**
     * @param Actor $i
     */
    public function iSeeElements($i)
    {
        foreach ($this->elements as $element) {
            $element->iSee($i);
        }
    }

    /**
     * @param Actor $i
     */
    public function iDontSeeElements($i)
    {
        foreach ($this->elements as $element) {
            $element->iDontSee($i);
        }
    }

}
