<?php

namespace WPDesk\Codeception\Tests\Acceptance\Elements;

use Codeception\Actor;

/**
 * Abstract HTML element.
 */
abstract class AbstractElement
{

    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    protected function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param Actor $i
     * @return void
     */
    abstract public function iSee($i);

    /**
     * @param Actor $i
     * @return void
     */
    abstract public function iDontSee($i);

}
