<?php

namespace WPDesk\Codeception\Tests\Acceptance\Elements;

use Codeception\Actor;

/**
 * HTML element.
 */
class Element extends AbstractElement
{

    /**
     * @var string
     */
    private $selector;

    /**
     * @var array
     */
    private $attributes;

    /**
     * Element constructor.
     *
     * @param string $selector .
     * @param array $attributes .
     * @param string $id .
     */
    public function __construct($selector, array $attributes = [], $id = '')
    {
        $this->selector = $selector;
        $this->attributes = $attributes;
        if (empty($id)) {
            $id = md5($selector, json_encode($attributes));
        }
        $this->setId($id);
    }

    /**
     * @return string
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param Actor $i
     * @return void
     */
    public function iSee($i)
    {
        $i->seeElement($this->selector, $this->attributes);
    }

    /**
     * @param Actor $i
     * @return void
     */
    public function iDontSee($i)
    {
        $i->dontSeeElement($this->selector, $this->attributes);
    }

}
