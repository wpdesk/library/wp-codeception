<?php

namespace WPDesk\Codeception\Tests\Acceptance\Elements;

use Codeception\Actor;

/**
 * Label element.
 */
class Label extends AbstractElement
{

    /**
     * @var string
     */
    private $text;

    /**
     * @var array|string
     */
    private $selector;

    /**
     * Label constructor.
     *
     * @param string $text .
     * @param array|string $selector .
     * @param string $id .
     */
    public function __construct($text, $selector = "", $id = '')
    {
        $this->selector = $selector;
        $this->text = $text;
        if (empty($id)) {
            $id = md5($text, json_encode($selector));
        }
        $this->setId($id);
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return array|string
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * @param Actor $i
     * @return void
     */
    public function iSee($i)
    {
        $i->see($this->text, $this->selector);
    }

    /**
     * @param Actor $i
     * @return void
     */
    public function iDontSee($i)
    {
        $i->dontSee($this->text, $this->selector);
    }

}
