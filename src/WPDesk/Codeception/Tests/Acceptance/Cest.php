<?php

namespace WPDesk\Codeception\Tests\Acceptance;

/**
 * Parent class for WPDesk acceptance tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 *
 * @deprecated Since version 1.4.
 * @deprecated Use Cest\Cest
 * @see \WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCest
 */
abstract class Cest extends Cest\AbstractCest
{

}
