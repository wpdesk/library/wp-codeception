<?php

namespace WPDesk\Codeception\Tests\Acceptance;

use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCestForHelper;


/**
 * Parent class for WPDesk Helper Tests.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 *
 * @deprecated Since version 1.4.
 * @deprecated Use AbstractCestForHelper
 * @see AbstractCestForHelper
 */
class HelperCest extends AbstractCestForHelper
{

}
