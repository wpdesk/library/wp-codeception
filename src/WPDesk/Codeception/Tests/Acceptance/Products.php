<?php

namespace WPDesk\Codeception\Tests\Acceptance;

/**
 * Woocomerce products data.
 *
 * @package WPDesk\Codeception\Tests\Acceptance
 */
class Products
{

    const PRODUCT_10  = 'product-10';
    const PRODUCT_9   = 'product-9';
    const PRODUCT_1   = 'product-1';
    const PRODUCT_09  = 'product-09';
    const PRODUCT_009 = 'product-009';
    const PRODUCT_001 = 'product-001';

    const PRODUCT_WITH_PRICE_10  = self::PRODUCT_10;
    const PRODUCT_WITH_PRICE_9   = self::PRODUCT_9;
    const PRODUCT_WITH_PRICE_1   = self::PRODUCT_1;
    const PRODUCT_WITH_PRICE_09  = self::PRODUCT_09;
    const PRODUCT_WITH_PRICE_009 = self::PRODUCT_009;
    const PRODUCT_WITH_PRICE_001 = self::PRODUCT_001;

    const PRODUCT_WITH_WEIGHT_10  = self::PRODUCT_10;
    const PRODUCT_WITH_WEIGHT_9   = self::PRODUCT_9;
    const PRODUCT_WITH_WEIGHT_1   = self::PRODUCT_1;
    const PRODUCT_WITH_WEIGHT_09  = self::PRODUCT_09;
    const PRODUCT_WITH_WEIGHT_009 = self::PRODUCT_009;
    const PRODUCT_WITH_WEIGHT_001 = self::PRODUCT_001;

}
