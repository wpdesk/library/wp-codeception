[![pipeline status](https://gitlab.com/wpdesk/wp-codeception/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-codeception/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-codeception/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-codeception/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-codeception/v/stable)](https://packagist.org/packages/wpdesk/wp-codeception) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-codeception/downloads)](https://packagist.org/packages/wpdesk/wp-codeception) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-codeception/v/unstable)](https://packagist.org/packages/wpdesk/wp-codeception) 
[![License](https://poser.pugx.org/wpdesk/wp-codeception/license)](https://packagist.org/packages/wpdesk/wp-codeception) 


Library for Codeception tests.
==============================

# PHP Documentation
https://wpdesk.gitlab.io/wp-codeception/index.html

# Installation

`composer require --dev wpdesk/wp-codeception`

# Usage

## Create codeception tests

`composer create-codeception-tests`

This command creates all folders and files required by codeception.

Additionally creates example test with WP Desk plugin activation: `tests/codeception/tests/acceptance/ActivationCest.php`

Required changes:
- change value of `WPDESK_PLUGIN_SLUG` variable in file: `tests/codeception/bootstrap.sh`

## Run tests

Run all tests:
`composer run-codeception-tests`

Run single test (whole class):
`composer run-codeception-tests ClassNameCest` (example: `composer run-codeception-tests WooCommerceCest`)

Run single test (single method):
`composer run-codeception-tests ClassNameCest:methodName` (example: `composer run-codeception-tests WooCommerceCest:makeOrder`)

Run all tests and do not shutdown docker-compose:
`composer run-codeception-tests all fast`

Run single test (whole class) and do not shutdown docker-compose:
`composer run-codeception-tests ClassNameCest fast` (example: `composer run-codeception-tests WooCommerceCest fast`)

Run single test (single method) and do not shutdown docker-compose:
`composer run-codeception-tests ClassNameCest:methodName fast` (example: `composer run-codeception-tests WooCommerceCest:makeOrder fast`)

Pass WooCommerce version to tests:
`composer run-codeception-tests all slow 3.7.0-beta.1`

or `composer run-codeception-tests all fast 3.7.0-beta.1`

or `composer run-codeception-tests ClassNameCest fast 3.7.0-beta.1`

## Run local tests - without docker

### Requirements
- mysql
  
  Create database `wptest`.
  
  Create user `mysql` with password `mysql`.
  

- apache
  
  Virtual host `wptests` with root folder `/tmp/wptests` 


- chromedriver
  
  On mac: `brew install chromedriver`

  If tests not working copy chromedriver to /usr/local/bin


- selenium-server

  On mac: `brew install selenium-server-standalone` and start selenium server.


- hosts
  
  Add to `/etc/hosts`: `127.0.0.1 wptests mysqltests chrome`

### Run
`composer run-local-codeception-tests`

# Writing tests

Acceptance tests: https://codeception.com/docs/03-AcceptanceTests

Recommended test format - Cest:
https://codeception.com/docs/07-AdvancedUsage

Wordpress and codeception:
https://codeception.com/for/wordpress

WebDriver (actions):
https://codeception.com/docs/modules/WebDriver

# Extending wp-codeception library

We have custom Tester Actions in TesterWordpressActions and TesterWooCommerceActions traits. 
All actions should be added there.

TesterWordpressActions and TesterWooCommerceActions traits are used in generated by codeception AcceptanceTester class.

# Other WP Desk plugins

When plugin uses other plugins and that plugins are in WordPress repository we can install them from repository in bootstrap.sh.

When we want to use plugin from wpdesk gitlab we can use prepare.sh script. 
There are example usage in flexible-shipping-pro plugin. Check flexible-shipping-pro/tests/codeception/prepare.sh.dist file.    

# Tips
If there is `unexpected alert open: {Alert text : }` add to acceptance.suite.yml in `WPWebDriver` config section:
```yaml
            capabilities:
                unexpectedAlertBehaviour: "accept"
```

#Development

After git checkout run commands:

`vendor/bin/codecept bootstrap tests/codeception`

`vendor/bin/codecept build` 
