#!/bin/bash

GIT_REPO=$1
CLONE_DIR=$2
BRANCH=$3

eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git clone $GIT_REPO $CLONE_DIR 
cd $CLONE_DIR
git checkout $BRANCH
git pull
