#!/bin/sh

SCRIPT_VERSION=16

start=`date +%s`

echo "Common bootstrap!"

WOOCOMMERCE_VERSION_PARAM=""

if [ -n "$WOOCOMMERCE_VERSION" ]
then
  WOOCOMMERCE_VERSION_PARAM="--version=$WOOCOMMERCE_VERSION"
fi

if [ ! -n "$WPDESK_PLUGIN_DIR" ]
then
  WPDESK_PLUGIN_DIR=$WPDESK_PLUGIN_SLUG
fi

WPDESK_CACHE_DIR_MAIN="${WPDESK_CACHE_DIR_MAIN:=/cache/wpdesk}"

PROJECT_DIR="${PROJECT_DIR:=$(pwd)}"

WP_ARGS="--path=$APACHE_DOCUMENT_ROOT --allow-root"

install_wordpress() {
  echo "Installing wordpress..."
  wp core install --url=${WOOTESTS_IP} --title=Woo-tests --admin_user=admin --admin_password=admin --admin_email=grola@seostudio.pl --skip-email $WP_ARGS
  wp rewrite structure '/%postname%/' $WP_ARGS
  if [ ! -f $APACHE_DOCUMENT_ROOT/.htaccess ]
  then
    cp -r ./vendor/wpdesk/wp-codeception/wordpress/htaccess $APACHE_DOCUMENT_ROOT/.htaccess
  fi

  echo "Uploads permissions..."
  mkdir -m 777 -p $APACHE_DOCUMENT_ROOT/wp-content/uploads/wpdesk-logs
  touch $APACHE_DOCUMENT_ROOT/wp-content/uploads/wpdesk-logs/wpdesk_debug.log
  chmod -R 777 $APACHE_DOCUMENT_ROOT

  echo "Enabling debug..."
  wp config set WP_DEBUG true --raw $WP_ARGS
  wp config set WP_DEBUG_LOG true --raw $WP_ARGS
  wp config set WP_DEBUG_DISPLAY true $WP_ARGS
}

install_woocommerce() {
  echo "Installing woocommerce..."
  if [ -n "$WOOCOMMERCE_VERSION" ] && [ "github" = "$WOOCOMMERCE_VERSION" ]
  then
    TMP_WORK_DIR=$(pwd)
    GITHUB_CACHE_DIR=/cache/github
    WOOCOMMERCE_GITHUB_CACHE_DIR=""$GITHUB_CACHE_DIR/woocommerce""
    changed=0
    if [ -d "$WOOCOMMERCE_GITHUB_CACHE_DIR" ]
    then
      cd $WOOCOMMERCE_GITHUB_CACHE_DIR
      git remote update && git status -uno | grep -q 'Your branch is behind' && changed=1
      if [ $changed = 1 ]; then
        git stash
        git pull
      fi
    else
      changed=1
      mkdir -p $GITHUB_CACHE_DIR
      cd $GITHUB_CACHE_DIR
      git clone https://github.com/woocommerce/woocommerce.git
    fi
    if [ $changed = 1 ]; then
      cd $WOOCOMMERCE_GITHUB_CACHE_DIR
      npm install
      composer install
      npm run build
    fi
    mkdir -p $APACHE_DOCUMENT_ROOT/wp-content/plugins/woocommerce
    rsync -az --exclude .git --exclude node_modules --exclude tests $WOOCOMMERCE_GITHUB_CACHE_DIR/ $APACHE_DOCUMENT_ROOT/wp-content/plugins/woocommerce/
    cd $TMP_WORK_DIR
  else
    wp plugin install woocommerce $WOOCOMMERCE_VERSION_PARAM $WP_ARGS
  fi
}

install_woocommerce_from_backup() {
  if [ -n "$WOOCOMMERCE_VERSION" ] && [ "github" = "$WOOCOMMERCE_VERSION" ]
  then
    install_woocommerce
  else
    echo "Installing woocommerce from backup..."
    cp -r $WOOCOMMERCE_CACHE_DIR/* $APACHE_DOCUMENT_ROOT/wp-content/plugins/woocommerce
  fi
}

backup_woocommerce() {
  echo "Backuping woocommerce..."
  rsync -az --exclude .git --exclude node_modules --exclude tests $APACHE_DOCUMENT_ROOT/wp-content/plugins/woocommerce/ $WOOCOMMERCE_CACHE_DIR/
}

install_themes() {
  echo "Installing storefront..."
  wp theme install storefront $WP_ARGS
  echo "Cloning storefront-wpdesk-tests..."
  mkdir -p $APACHE_DOCUMENT_ROOT/wp-content/themes/storefront-wpdesk-tests
  sh ./vendor/wpdesk/wp-codeception/scripts/clone.sh https://gitlab.com/wpdesk/storefront-wpdesk-tests.git $APACHE_DOCUMENT_ROOT/wp-content/themes/storefront-wpdesk-tests
}

activate_theme() {
  echo "Activating storefront-wpdesk-tests..."
  wp theme activate storefront-wpdesk-tests $WP_ARGS
}

backup_themes() {
  echo "Backuping themes..."
  mkdir -p $THEMES_CACHE_DIR
  cp -r $APACHE_DOCUMENT_ROOT/wp-content/themes/* $THEMES_CACHE_DIR
}

install_themes_from_backup() {
  echo "Installing themes from backup..."
  cp -r $THEMES_CACHE_DIR $APACHE_DOCUMENT_ROOT/wp-content
}

backup_uploads() {
  echo "Backuping uploads..."
  mkdir -p $UPLOADS_CACHE_DIR
  cp -r $APACHE_DOCUMENT_ROOT/wp-content/uploads/* $UPLOADS_CACHE_DIR
}

install_uploads_from_backup() {
  echo "Installing uploads from backup..."
  cp -r $UPLOADS_CACHE_DIR $APACHE_DOCUMENT_ROOT/wp-content
}

create_product() {
    echo "Create WC product $1 $2"
    dimensions='{"width":"'$2'","length":"'$2'","height":"'$2'"}'
    wp wc product create \
        --name="Product $1" \
        --virtual=false \
        --downloadable=false \
        --type=simple \
        --sku=product-$1 \
        --regular_price=$2 \
        --weight=$2 \
        --dimensions=$dimensions \
        --user=admin  $WP_ARGS
}

configure_woocommerce() {
  echo "Create WC products"
  create_product 100 100
  create_product 10 10
  create_product 9 9
  create_product 1 1
  create_product 09 0.9
  create_product 009 0.09
  create_product 001 0.01
  create_product 0001 0.001
  create_product 00001 0.0001

  echo "Updating options..."
  wp option update woocommerce_admin_notices '{}' --format=json $WP_ARGS
  wp option update storefront_nux_dismissed 1 $WP_ARGS
  wp option set woocommerce_store_address "al. Jana Pawła 12" $WP_ARGS
  wp option set woocommerce_store_address_2 "" $WP_ARGS
  wp option set woocommerce_store_city "Warszawa" $WP_ARGS
  wp option set woocommerce_default_country "PL" $WP_ARGS
  wp option set woocommerce_store_postalcode "22-100" $WP_ARGS
  wp option set woocommerce_currency "PLN" $WP_ARGS
  wp option set woocommerce_currency_pos "right_space" $WP_ARGS
  wp option set woocommerce_product_type "physical" $WP_ARGS
  wp option set woocommerce_allow_tracking "no" $WP_ARGS
  wp option set --format=json woocommerce_stripe_settings '{"enabled":"no","create_account":false,"email":false}' $WP_ARGS
  wp option set --format=json woocommerce_ppec_paypal_settings '{"reroute_requests":false,"email":false}' $WP_ARGS
  wp option set --format=json woocommerce_cheque_settings '{"enabled":"no"}' $WP_ARGS
  wp option set --format=json woocommerce_bacs_settings '{"enabled":"no"}' $WP_ARGS
  wp option set --format=json woocommerce_cod_settings '{"enabled":"yes"}' $WP_ARGS
  wp option set --format=json woocommerce_onboarding_profile '{"skipped":true}' $WP_ARGS
  wp option set --format=json wc-admin-onboarding-profiler-reminder '{"skipped":true}' $WP_ARGS
  wp option get  woocommerce_onboarding_profile $WP_ARGS
  wp option set woocommerce_task_list_hidden "yes" $WP_ARGS

  echo "Creating taxes..."
  wp wc tax create --country="PL" --rate=23 --name=VAT --shipping=true --user=admin $WP_ARGS
  wp option set woocommerce_calc_taxes "yes" $WP_ARGS

  echo "Creating shipping methods..."
  wp wc shipping_zone_method create 0 --method_id="flat_rate" --settings='{"title": "Flat rate", "cost":1, "tax_status": "taxable"}' --enabled=true --user=admin $WP_ARGS

  echo "Create WooCommerce pages..."
  wp wc --user=admin tool run install_pages $WP_ARGS

  echo "Create Customer..."
  wp wc customer create --email='customer@woo.local' --username="customer" --billing='{"first_name":"First","last_name":"Last","company":"WPDesk","address_1":"Street 1","city":"City","postcode": "53-030", "country": "PL", "phone": "012345678"}' --password='customer' --user=admin  $WP_ARGS

  echo "Disable REST API permissions"
  wp option set wpdesk_rest_api_disable_permissions "1" $WP_ARGS
}

make_database_backup() {
  echo "Create database backup..."
  mkdir -p $DB_CACHE_DIR
  wp db export $DB_BACKUP_FILE --add-drop-table $WP_ARGS
}

import_database() {
  echo "Importing database..."
  wp db import $DB_BACKUP_FILE --add-drop-table $WP_ARGS
}

install_project_as_plugin() {
    if [ -d $PROJECT_DIR ]
    then
      echo "Copying files..."
      rm -rf $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR
      mkdir -p $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR
      rsync -az --exclude .git --exclude node_modules --exclude tests  $PROJECT_DIR/ $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR/
      TMP_WORK_DIR=$(pwd)
      cd $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR
      composer install --no-dev
      cd $TMP_WORK_DIR
    fi
    if [ -d $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR/tests ]
    then
      rm -r $APACHE_DOCUMENT_ROOT/wp-content/plugins/$WPDESK_PLUGIN_DIR/tests
    fi
}

activate_plugin() {
    echo "Activating $WPDESK_PLUGIN_DIR..."
    wp plugin activate $WPDESK_PLUGIN_DIR $WP_ARGS
}

activate_woocommerce() {
    echo "Activating woocommerce..."
    wp plugin activate woocommerce $WP_ARGS
}

set_variables() {

    WP_VERSION=`wp core version $WP_ARGS`
    WOOCOMMERCE_VERSION=`wp plugin get woocommerce --field=version $WP_ARGS`

    VERSION_PARAMETER="$WPDESK_PLUGIN_SLUG-$WP_VERSION-$WOOCOMMERCE_VERSION-$SCRIPT_VERSION"

    WPDESK_CACHE_DIR="$WPDESK_CACHE_DIR_MAIN/$VERSION_PARAMETER"
    mkdir -p $WPDESK_CACHE_DIR

    DB_CACHE_DIR=$WPDESK_CACHE_DIR/db
    DB_BACKUP_FILE=$DB_CACHE_DIR/db.sql

    WOOCOMMERCE_CACHE_DIR=$WPDESK_CACHE_DIR/woocommerce

    THEMES_CACHE_DIR=$WPDESK_CACHE_DIR/themes

    UPLOADS_CACHE_DIR=$WPDESK_CACHE_DIR/uploads
}

install_project_as_plugin

if ! $(wp core is-installed $WP_ARGS);
then

  install_wordpress

  install_woocommerce

  set_variables

  if [ -d $THEMES_CACHE_DIR ]
  then
    install_themes_from_backup
  else
    install_themes
    backup_themes
  fi

  activate_theme

  if [ -f $DB_BACKUP_FILE ]
  then
      import_database
      wp option update siteurl "http://{$WOOTESTS_IP}" $WP_ARGS
      wp option update home "http://{$WOOTESTS_IP}" $WP_ARGS
  else
      activate_woocommerce
      configure_woocommerce
      activate_plugin
      make_database_backup
  fi

  if [ -d $UPLOADS_CACHE_DIR ]
  then
    install_uploads_from_backup
  else
    backup_uploads
  fi

  echo "Copy database backup to project"
  pwd
  mkdir -p tests/codeception/tests/_data
  cp -r $DB_BACKUP_FILE tests/codeception/tests/_data/db.sql

fi


echo
end=`date +%s`
echo "Common bootstrap duration: $((end-start)) seconds elapsed.."
echo
