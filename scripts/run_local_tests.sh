#!/bin/sh

export APACHE_DOCUMENT_ROOT="${APACHE_DOCUMENT_ROOT:=/tmp/wptests}"
export DBNAME="${DBNAME:=wptest}"
export DBUSER="${DBUSER:=mysql}"
export DBPASS="${DBPASS:=mysql}"
export DBHOST="${DBHOST:=mysqltests}"
export PROJECT_DIR="${PROJECT_DIR:=$(pwd)}"
export WOOTESTS_IP="${WOOTESTS_IP:=wptests}"
export WPDESK_CACHE_DIR_MAIN="${WPDESK_CACHE_DIR_MAIN:=/tmp/cache/wpdesk}"
export DEPENDENT_PLUGINS_DIR="${DEPENDENT_PLUGINS_DIR:=$(pwd)/..}"

TMP_WORK_DIR=$(pwd)

mkdir -p $APACHE_DOCUMENT_ROOT
cd $APACHE_DOCUMENT_ROOT
wp core download
wp config create --dbname=$DBNAME --dbuser=$DBUSER --dbhost=$DBHOST --dbpass=$DBPASS
wp db reset --yes

cd $TMP_WORK_DIR

PWD

if [ -n "$2" ]
then
  export $WOOCOMMERCE_VERSION="$2"
fi

. ./tests/codeception/bootstrap.sh

./vendor/bin/codecept clean
./vendor/bin/codecept run -f --steps --html --verbose acceptance $1
