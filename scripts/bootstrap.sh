#!/bin/bash

export WPDESK_PLUGIN_SLUG=enter-plugin-slug-here
export WPDESK_PLUGIN_DIR=enter-plugin-dir-here
export WPDESK_PLUGIN_FILE=enter-plugin-file-here
export WPDESK_PLUGIN_PRODUCT_ID="Enter plugin product id here"
export WPDESK_PLUGIN_TITLE="Enter plugin title here"

export WOOTESTS_IP=${WOOTESTS_IP:wootests}

export DEPENDENT_PLUGINS_DIR="${DEPENDENT_PLUGINS_DIR:=$(pwd)/..}"

sh ./vendor/wpdesk/wp-codeception/scripts/common_bootstrap.sh
