# WP Codeception Changelog

## [2.12.1] - 2025-02-05
### Fixed
- AbstractCestForWooCommerce test class run makeOrder test twice on the same checkout type (block checkout)
- Disable comming soon feature for testing WordPress instance

## [2.12.0] - 2024-11-29
### Added
- common WordPress tests with translations test 

## [2.11.13] - 2024-08-26
### Fixed
- add shipping method in shipping zone

## [2.11.12] - 2024-08-23
### Fixed
- disabled linter in CI

## [2.11.11] - 2024-08-22
### Fixed
- makeOrder() function country selector

## [2.11.10] - 2024-08-20
### Fixed
- Selector of country field in checkout

## [2.11.9] - 2024-08-14
### Fixed
- Country field fill function fixed for WooCommerce 9.2

## [2.11.8] - 2024-07-30
### Fixed
- Fill country field in block checkout

## [2.11.7] - 2024-06-26
### Changed
- Avoid downloading additional content (themes and plugins) when checking out WordPress environment with prepare-wordpress-for-codeception command.

## [2.11.6] - 2024-05-15
### Fixed
- WordPress 6.5 compatibility (plugin activation and deactivation)

## [2.11.5] - 2024-03-20
### Fixed
- checkout block test address fields

## [2.11.4] - 2024-03-20
### Fixed
- filesystem method in wp-config for woocommerce logger

## [2.11.3] - 2024-03-18
### Fixed
- checkout block test

## [2.11.2] - 2024-02-23
### Fixed
- wp-browser version: https://wpbrowser.wptestkit.dev/migration/

## [2.11.1] - 2024-01-09
### Fixed
- Prepare database command
- 
## [2.11.0] - 2023-12-28
### Added
- Added steps to create shipping methods in shipping zones
### Changed
- switch cart and checkout methods uses database update command

## [2.10.0] - 2023-12-14
### Added
- WooCommerce block cart and checkout tests 

## [2.9.0] - 2023-12-07
### Added
- Cart and Checkout commands: switchCartAndCheckoutToOldVersion, switchCartAndCheckoutToBlocks
- Prepare database command prepares old version of cart and checkout

## [2.8.6] - 2023-11-16
### Fixed
- WooCommerce order placing test compatibility with wp_wc_orders and wp_posts

## [2.8.5] - 2023-11-14
### Fixed
- Update custom Storefront theme reference.

## [2.8.4] - 2023-11-08
### Fixed
- Orders total no longer stored in postmeta

## [2.8.3] - 2023-10-30
### Added
- TesterWooCommerceCheckoutBlockTrait

## [2.8.2] - 2023-06-22
### Fixed
- Downgraded phpdotenv to ^4 - codeception do not see env vars

## [2.8.1] - 2023-06-20
### Fixed
- Checkout fields filling order to prevent multiple AJAX requests

## [2.8.0] - 2023-06-02
### Changed
- Improved changelog format
- Vetted required packages

## [2.7.10] - 2023-02-09
### Revert
- Faster local testing

## [2.7.8] - 2023-02-09
### Fixed
- Faster local testing

## [2.7.6] - 2022-11-02
### Fixed
- Fixed class for Proceed to checkout button

## [2.7.5] - 2022-10-05
### Fixed
- composer commands should return 0 from execute method

## [2.7.4] - 2022-06-17
### Fixed
- WooCommerce order placing test

## [2.7.2] - 2022-05-20
### Added
- woocommerce option - speed up tests

## [2.7.1] - 2022-05-12
### Added
- tests site URL
- CloudFlare Flexible SSL

## [2.6.6] - 2022-05-12
### Fixed
- use external wp-cli

## [2.6.5] - 2022-04-12
### Fixed
- fatal error in unit tests
- fatal error during prepare tests

## [2.6.4] - 2022-03-10
### Fixed
- empty cart

## [2.6.2] - 2022-03-09
### Fixed
- gherkin steps

## - [2.6.1] - 2022-03-02
### Added
- codeception/module-asserts library

## [2.6.0] - 2022-02-08
### Added
- Database update in loginAsAdministrator step

## [2.5.3] - 2022-01-28
### Added
- Code coverage in local tests

## [2.4.5] - 2022-01-03
### Fixed
- Rewrite structure

## - [2.4.4] - 2021-09-28
### Fixed
- Add products to cart method

## [2.4.3] - 2021-09-28
### Fixed
- Product author
- Plugin slug in wp cli
- Reverted storefront to current version

## [2.4.2] - 2021-09-27
### Fixed
- Force install storefront in version 3.8.1

## [2.4.1] - 2021-09-19
### Removed
- wpdesk/wp-builder library

## [2.4.0] - 2021-09-15
### Added
- bordoni/phpass dependency instead of removed hautelook/phpass

## [2.3.3] - 2021-08-24
### Added
- support for additional theme files

## [2.3.2] - 2021-08-23
### Fixed
- plugin directory

## [2.3.1] - 2021-07-23
### Fixed
- tests stability - add product to cart

## [2.3.0] - 2021-07-02
### Added
- parallel tests in CI

## [2.2.3] - 2021-07-09
### Added
- Improves in place order step

## [2.2.2] - 2021-06-04
### Added
- New Gherkin steps - costs for flat rate

## [2.2.1] - 2021-06-04
### Added
- New Gherkin steps

## [2.2.0] - 2021-06-01
### Added
- New Gherkin steps

## [2.1.1] - 2021-05-27
### Fixed
- WooCommerce Order Test

## [2.1.0] - 2021-05-18
### Added
- New Gherkin steps

## [2.0.1] - 2021-05-11
### Fixed
- fixed local path for rsync

## [2.0.0] - 2021-05-04
### Added
- local tests without docker
- new composer commands
- new flow for codeception tests

## [1.11.0] - 2021-04-21
### Added
- Support for Composer 2
- Support for PHP 8

## [1.10.5] - 2021-03-04
### Fixed
- fixed random failures on checkout pages

## [1.10.4] - 2021-03-04
### Fixed
- local output data cleaning

## [1.10.3] - 2021-02-18
### Added
- clean tests output directory before new test on local testing

## [1.10.2] - 2021-02-17
### Fixed
- copying project files

## [1.10.0] - 2021-02-15
### Added
- local testing

## [1.9.2] - 2021-02-09
### Changed
- Script Version

## [1.9.1] - 2021-01-22
### Added
- speed improvements

## [1.9.0] - 2020-12-29
### Added
- coupons functions

## [1.8.2] - 2020-12-07
### Fixed
- removed session saving

## [1.8.1] - 2020-12-03
### Fixed
- saved sessions checking

## [1.8.0] - 2020-12-01
### Added
- -f parameter: stop after first failure

## [1.7.0] - 2020-11-29
### Added
- executeWpCli method

## [1.6.6] - 2020-11-23
### Changed
- Speed improvements

## [1.6.2] - 2020-11-16
### Fixed
- Error "Checkbox by Label or CSS or XPath element with 'ship_to_different_address' was not found."

## [1.6.1] - 2020-11-06
### Changed
- docker chrome image

## [1.6.0] - 2020-10-22
### Changed
- logOut method to goToLoginPageAndLogOut

## [1.5.25] - 2020-05-13
### Added
- vlucas/phpdotenv library to requirements

## [1.5.24] - 2020-04-21
### Added
- configuration file for recorder

## [1.5.23] - 2020-04-15
### Fixed
- logout function

## [1.5.22] - 2020-04-15
### Fixed
- logout function

## [1.5.21] - 2020-04-15
### Added
- logout function

## [1.5.20] - 2020-04-13
### Fixed
- Flexible Shipping Method

## [1.5.19] - 2020-04-13
### Added
- required libraries

## [1.5.18] - 2020-04-13
### Added
- required libraries

## [1.5.17] - 2020-04-06
### Fixed
- addFlexibleShippingMethod

## [1.5.16] - 2020-04-06
### Added
- flexible shipping methods

## [1.5.15] - 2020-03-31
### Added
- shipping method in makeOrder

## [1.5.14] - 2020-03-30
### Added
- WPDESK_PLUGIN_DIR

## [1.5.13] - 2020-03-30
### Fixed
- removed session cache

## [1.5.12] - 2020-03-30
### Added
- uploads cache

## [1.5.11] - 2020-03-30
### Added
- WooCommerce tools via REST API

## [1.5.10] - 2020-03-30
### Added
- speed up admin and customer logging in

## [1.5.9] - 2020-03-30
### Added
- cache per project

## [1.5.8] - 2020-03-30
### Fixed
- plugin activation after database backup

## [1.5.7] - 2020-03-29
### Fixed
- database dump directory
- wait for ajax renderer not visible

## [1.5.4] - 2020-03-29
### Added
- MYSQL_IP in docker

## [1.5.3] - 2020-03-29
### Fixed
- removed delete_old_output_files

## [1.5.2] - 2020-03-29
### Fixed
- place order

## [1.5.1] - 2020-03-29
### Fixed
- place order

## [1.5.0] - 2020-03-19
### Added
- WooCommerce shipping methods functionality

## [1.4.19] - 2020-03-16
### Fixed
- fixed db.sql copy to project
- theme activation

## [1.4.18] - 2020-03-16
### Fixed
- fixed siteurl in imported database

## [1.4.17] - 2020-03-15
### Added
- next attempt to cache

## [1.4.16] - 2020-03-07
### Added
- shm_size in chrome docker instance

## [1.4.15] - 2020-01-24
### Fixed
- git stash in woocommerce from github

## [1.4.11] - 2020-01-21
### Fixed
- speed optimisations

## [1.4.10] - 2020-01-21
### Fixed
- helper cest

## [1.4.9] - 2019-12-14
### Fixed
- disabled cache

## [1.4.8] - 2019-12-13
### Fixed
- removed saved sessions

## [1.4.7] - 2019-12-12
### Fixed
- uncheck -> uncheckOption

## [1.4.6] - 2019-12-11
### Added
- uncheck Ship to diferent address on checkout

## [1.4.5] - 2019-12-06
### Fixed
- unexpected alert open: {Alert text : }

## [1.4.4] - 2019-12-06
### Added
- reports and more verbose when runs form composer command

## [1.4.3] - 2019-12-05
### Fixed
- Themes cache

## [1.4.2] - 2019-11-18
### Added
- Intensive cache usage

## [1.4.1] - 2019-11-12
### Fixed
- Action Links fields visibility - protected

## [1.4.0] - 2019-11-11
### Modified
- code refactor

## [1.3.0] - 2019-10-31
### Added
- activation date option test

## [1.2.7] - 2019-10-21
### Added
- tracker tests class WpdeskTracker

## [1.2.6] - 2019-09-13
### Added
- can fill custom fields

## [1.2.5] - 2019-09-12
### Added
- Use local cache for github

## [1.2.3] - 2019-09-12
### Added
- WooCommerce from github

## [1.2.2] - 2019-09-11
### Added
- default billing data

## [1.2.1] - 2019-08-30
### Fixed
- backward compatibility

## [1.2.0] - 2019-08-30
### Added
- new helpers to tester added

## [1.1.13] - 2019-08-27
### Added
- more waits for clickable elements
- products class

## [1.1.12] - 2019-08-26
### Added
- emptyCart method

## [1.1.11] - 2019-08-22
### Added
- products weights

## [1.1.10] - 2019-08-09
### Added
- strorefront-wpdesk-tests theme

## [1.1.9] - 2019-08-09
### Fixed
- Typo

## [1.1.8] - 2019-08-08
### Added
- Enabled debug
- .gitignore in `_generated`

## [1.1.7] - 2019-08-02
### Added
- Added ability to use other wpdesk plugins

## [1.1.6] - 2019-08-01
### Added
- Added WOOCOMMERCE_VERSION variable

## [1.1.5] - 2019-07-31
### Added
- Removed ports from docker-compose

## [1.1.4] - 2019-07-31
### Added
- Common WooCommerce test

## [1.1.3] - 2019-07-30
### Added
- wp cli debug in plugin activation

## [1.1.2] - 2019-07-30
### Added
- Fixed activation

## [1.1.0] - 2019-07-30
### Added
- Production version

## [1.0.0] - 2019-05-30
### Added
- First version
